<section class="unpad">
    <div class="row--gapless">
        <div class="col-sm-4 col-xs-12">
            <a href="list_paten.php" class="block">
                <div class="feature feature-7 boxed text-center imagebg" data-overlay="3">
                    <div class="background-image-holder">
                        <img alt="background" src="assets/img/penelitian.jpg" />
                    </div>
                    <h4 class="pos-vertical-center"><strong>Daftar Paten</strong></h4>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-xs-6">
            <a href="panduan_peraturan.php" class="block">
                <div class="feature feature-7 boxed text-center imagebg" data-overlay="5">
                    <div class="background-image-holder">
                        <img alt="background" src="assets/img/519476132.jpg" />
                    </div>
                    <h4 class="pos-vertical-center"><strong>Buku Panduan</strong></h4>
                </div>
            </a>
        </div>
        <div class="col-sm-4 col-xs-6">
            <a href="pelatihan.php" class="block">
                <div class="feature feature-7 boxed text-center imagebg" data-overlay="5">
                    <div class="background-image-holder">
                        <img alt="background" src="assets/img/pengabdian.jpg" />
                    </div>
                    <h4 class="pos-vertical-center"><strong>Pelatihan HKI</strong></h4>
                </div>
            </a>
        </div>
    </div>
</section>

<footer class="footer-5 text-center-xs space--xs bg--dark space-bottom--sm">
    <section class="switchable unpad">
        <div class="container">
            <div class="row footer">
                <div class="col-xs-12 col-sm-12 col-md-3 text-center-sm">
                    <a href="/haki_polinema_2/">
                        <img class="footer-logo" alt="logo" src="assets/img/hki-logo.png">
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-widget widget_nav_menu clr">
                        <div class="menu-menu-footer-1-container">
                            <ul class="menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                                    <a href="#">MENU</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="profil.php">Profil</a> </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="tentang.php">Tentang</a> </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="pelatihan.php">Pelatihan HKI</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-widget widget_nav_menu clr">
                        <div class="menu-menu-footer-1-container">
                            <ul class="menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                                    <a href="#">LAINNYA</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="list_paten.php">Daftar Paten</a> </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="panduan_peraturan.php">Buku Panduan</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="footer-widget widget_nav_menu clr">
                        <div class="menu-menu-footer-1-container">
                            <ul class="menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                                    <a href="#">PRANALA</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://polinema.ac.id">Polinema</a> </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://akademik.polinema.ac.id">SIAKAD</a> </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://library.polinema.ac.id/">Perpustakaan</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <span class="type--fine-print">
                    Copyright &copy;
                    <span class="update-year">
                        <script>document.write(new Date().getFullYear());</script>
                    </span>
                    All Rights Reserved<br>This website is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="http://polinema.ac.id">Politeknik Negeri Malang</a>
                </span>
            </div>
            <div class="col-sm-6 text-right text-center-xs">
                <span class="type--fine-print">HAKI Politeknik Negeri Malang</span>
            </div>
        </div>
    </div>
</footer>