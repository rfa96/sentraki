<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/17/18
 * Time: 02:33 AM
 */
    include "koneksi.php";
    $id_agenda = $_GET['id_agenda'];
    $sql_agenda = $conn->query("SELECT * FROM agenda WHERE id_agenda = ".$id_agenda);
    $data_agenda = $sql_agenda->fetch_array();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "head_tag.php";?>
        <title>View Agenda - HAKI Polinema</title>
    </head>
    <body class="page-template-default page page-id-53 page-child parent-pageid-5" data-smooth-scroll-offset="80">
        <?php include "navbar.php";?>

        <div class="main-container">
            <?php include "carousel.php";?>

            <section class="space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="text-center"><?= $data_agenda[1]?></h1>
                            <p class="text-center" align="justify"><?= "Diunggah pada: ".date("d-M-Y", strtotime($data_agenda[3]))." Jam ".$data_agenda[4]?></p>
                            <?= $data_agenda[2];?>
                        </div>
                    </div>
                </div>
            </section>


            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
    </body>
</html>
