<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/13/18
 * Time: 22:50 PM
 */
    include "koneksi.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "head_tag.php"?>
        <title>Panduan dan Peraturan - HAKI Polinema</title>
    </head>
    <body class="page-template-default page page-id-53 page-child parent-pageid-5" data-smooth-scroll-offset="80">
        <?php include "navbar.php";?>

        <div class="main-container">
            <?php include "carousel.php";?>
            <section class="text-center space--xs bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                            <h1>Panduan dan Peraturan</h1>
                            <p class="text-center">Berikut ini adalah berbagai panduan dan peraturan yang dapat diunduh.</p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="space--sm bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="success">
                                        <th>No.</th>
                                        <th>Nama Panduan</th>
                                        <th>Tahun Terbit</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $nomor = 1;
                                        $sql_panduan = $conn->query("SELECT * FROM panduan");
                                        while($data_panduan = $sql_panduan->fetch_array())
                                        {
                                            ?>
                                                <tr>
                                                    <td><?= $nomor++;?></td>
                                                    <td><?= $data_panduan[1]?></td>
                                                    <td><?= $data_panduan[2]?></td>
                                                    <td align="center"><a href="assets/wp-contents/panduan/<?= $data_panduan[3]?>" class="btn btn-primary"><span class="btn-line btn--beta">Unduh</span></a> </td>
                                                </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
    </body>
</html>