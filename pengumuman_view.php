<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/12/18
 * Time: 10:22 AM
 */
    include "koneksi.php";
    $id_pengumuman = $_GET['id_pengumuman'];
    $sql_pengumuman = $conn->query("SELECT * FROM pengumuman WHERE id_pengumuman = ".$id_pengumuman);
    $data_pengumuman = $sql_pengumuman->fetch_array();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "head_tag.php";?>
        <title>Tampil Pengumuman - HAKI Polinema</title>
    </head>
    <body class="post-template-default single single-post postid-118 single-format-standard" data-smooth-scroll-offset='80'>
        <?php include "navbar.php";?>

        <!--- Content -->
        <div class="main-container">
            <?php include "carousel.php";?>
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
                            <article>
                                <div class="article__title text-center">
                                    <h1 class="h2"><?= $data_pengumuman[1]?></h1>
                                    <span>Diunggah pada <?= date("d M Y", strtotime($data_pengumuman[3]))?> jam <?= $data_pengumuman[4]?></span>
                                </div>
                                <div class="article__body">
                                    <?php
                                        if(!empty($data_pengumuman[5]))
                                        {
                                            ?>
                                                <img src="assets/wp-contents/<?= $data_pengumuman[5]?>">
                                            <?php
                                        }
                                    ?>
                                    <p><?= $data_pengumuman[2]?></p>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>
            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
    </body>
</html>