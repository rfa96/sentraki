<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/10/18
 * Time: 06:36 AM
 */
    include "koneksi.php";
    $id_berita = $_GET['id_berita'];
    $sql_berita = $conn->query("SELECT * FROM berita WHERE id_berita = ".$id_berita);
    $konten = $sql_berita->fetch_array();
?>
<!DOCTYPE html>
<html lang="id-ID">
    <head>
        <?php include "head_tag.php";?>
        <title>Tampil Berita - HAKI Polinema</title>
    </head>
    <body class="post-template-default single single-post postid-118 single-format-standard" data-smooth-scroll-offset='80'>
        <?php include "navbar.php";?>

        <!--- Content -->
        <div class="main-container">
            <?php include "carousel.php";?>
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-10 col-sm-offset-1 col-md-offset-1">
                            <article>
                                <div class="article__title text-center">
                                    <h1 class="h2"><?= $konten[2]?></h1>
                                    <span>Diunggah pada <?= date("d M Y", strtotime($konten[5]))?> jam <?= $konten[6]?></span>
                                </div>
                                <div class="article__body">
                                    <?php
                                        if($konten[4])
                                        {
                                            ?>
                                                <center>
                                                    <img src="assets/wp-contents/<?= $konten[4]?>"/>
                                                </center>
                                            <?php
                                        }
                                    ?>
                                    <p><?= $konten[3]?></p>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>

            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
    </body>
</html>
