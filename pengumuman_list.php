<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/18/18
 * Time: 15:26 PM
 */
    include "koneksi.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "head_tag.php"?>
        <title>Arsip Pengumuman - HAKI Polinema</title>
        <link rel="stylesheet" href="assets/css/kartu_arsip.css"/>
    </head>
    <body class="page-template-default page page-id-53 page-child parent-pageid-5" data-smooth-scroll-offset="80">
        <?php include "navbar.php";?>

        <div class="main-container">
            <?php include "carousel.php";?>

            <section class="space--xs">
                <div class="container">
                    <div class="row">
                        <h1 class="text-center">ARSIP PENGUMUMAN</h1>
                        <?php
                            if(isset($_GET['page']))
                            {
                                $page = $_GET['page'];
                            }
                            else
                            {
                                $page = 1;
                            }

                            if($page == '' || $page == 1)
                            {
                                $page1 = 0;
                            }
                            else
                            {
                                $page1 = ($page * 5) - 5;
                            }

                            $sql_pengumuman = $conn->query("SELECT * FROM pengumuman ORDER BY tanggal DESC LIMIT $page1, 5");
                            while($row_pengumuman = $sql_pengumuman->fetch_array())
                            {
                                ?>
                                    <div class="card-2">
                                        <div class="card__body">
                                            <font size="5"><?= $row_pengumuman[1]?></font><br>
                                            <a href="pengumuman_view.php?id_pengumuman=<?= $row_pengumuman[0]?>">
                                                Selengkapnya >>>
                                            </a>
                                        </div>
                                    </div><br>
                                <?php
                            }

                            $sql_pengumuman_all = $conn->query("SELECT * FROM pengumuman");
                            $records = $sql_pengumuman_all->num_rows;
                            $records_pages = $records / 5;
                            $records_pages = ceil($records_pages);
                            $prev = $page - 1;
                            $next = $page + 1;
                        ?>
                        <br>
                        <center>
                            <nav aria-label="Page navigation">
                                <ul class="pagination pagination-lg">
                                    <?php
                                        if($page >= 5)
                                        {
                                            ?>
                                                <li>
                                                    <a href="?page=1" aria-label="Previous">
                                                        <span aria-hidden="true">First</span>
                                                    </a>
                                                </li>
                                            <?php
                                        }

                                        if($prev >= 1)
                                        {
                                            ?>
                                                <li>
                                                    <a href="?page=<?= $prev?>" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </li>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                                <li class="disabled">
                                                    <a aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </li>
                                            <?php
                                        }

                                        if($records_pages >= 0)
                                        {
                                            for($r = 1; $r <= $records_pages; $r++)
                                            {
                                                $active = $r == $page ? 'class="active"' : '';
                                                ?>
                                                    <li><a href="?page=<?= $r?>"><?= $r?></a></li>
                                                <?php
                                            }
                                        }

                                        if($next <= $records_pages && $records_pages >= 2)
                                        {
                                            ?>
                                                <li>
                                                    <a href="?page=<?= $next;?>" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                    </a>
                                                </li>
                                            <?php
                                        }
                                        else
                                        {
                                            ?>
                                                <li class="disabled">
                                                    <a aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                    </a>
                                                </li>
                                            <?php
                                        }

                                        if($page != $records_pages && $records_pages >= 5)
                                        {
                                            ?>
                                                <li>
                                                    <a href="?page=<?= $records_pages?>" aria-label="Next">
                                                        <span aria-hidden="true">Last</span>
                                                    </a>
                                                </li>
                                            <?php
                                        }
                                    ?>
                                </ul>
                            </nav>
                        </center>
                    </div>
                </div>
            </section>
            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
    </body>
</html>
