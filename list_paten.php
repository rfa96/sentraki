<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/12/18
 * Time: 18:11 PM
 */
    include "koneksi.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "head_tag.php"?>
        <title>Daftar Paten - HAKI Polinema</title>
        <link rel="stylesheet" type="text/css" href="assets/css/datatables_bs4.min.css"/>
        <script type="text/javascript" src="assets/js/datatables_bs4.min.js"></script>
    </head>
    <body class="page-template-default page page-id-53 page-child parent-pageid-5" data-smooth-scroll-offset="80">
        <?php include "navbar.php";?>

        <div class="main-container">
            <?php include "carousel.php";?>

            <section class="space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="text-center">Daftar Paten</h1>
                            <table id="daftar_paten" class="reg-table display">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nomor Paten</th>
                                        <th>Nama Inventor</th>
                                        <th>Judul Paten</th>
                                        <th>Jenis Paten</th>
                                        <th>Tahun</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $nomor = 1;
                                        $sql_paten = $conn->query("SELECT * FROM paten");
                                        while($data_paten = $sql_paten->fetch_array())
                                        {
                                            ?>
                                                <tr>
                                                    <td><?= $nomor++?></td>
                                                    <td><?= $data_paten[4]?></td>
                                                    <td><?= $data_paten[1]?></td>
                                                    <td>
                                                        <?php
                                                            if(!empty($data_paten[2]))
                                                            {
                                                                ?>
                                                                    <a href="assets/wp-contents/paten/<?= $data_paten[6]?>" target="_blank"><?= $data_paten[2]?></a>
                                                                <?php
                                                            }
                                                            else
                                                            {
                                                                echo $data_paten[2];
                                                            }
                                                        ?>
                                                    </td>
                                                    <td><?= $data_paten[3]?></td>
                                                    <td><?= $data_paten[5]?></td>
                                                </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
        <script>
            $(document).ready(function () {
                $("#daftar_paten").DataTable();
            });
        </script>
    </body>
</html>
