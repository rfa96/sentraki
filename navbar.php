<a id="start"></a>

<div class="nav-container bg--dark">
    <div class="bar bar--sm visible-xs bg--dark bg--navy">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-2 unpad">
                    <a href="/"><img class="logo logo-light" alt="logo" src="assets/img/hki-logo-light-horz.png"></a>
                </div>
                <div class="col-xs-6 col-sm-10 text-right">
                    <a href="/" class="mobile-menu-toggle" data-toggle-class="body;menu-is-open|#menu1;hidden-xs hidden-sm">
                        <div class="site-header__menu-bars"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <nav id="menu1" class="bar bar-2 hidden-xs bar--transparent bg--navy" data-scroll-class='90vh:pos-fixed'>
        <div class="site-header__wrap">
            <div class="hidden-xs">
                <div class="bar__module">
                    <a href="/"><img class="logo logo-light" alt="logo" src="assets/img/hki-logo-light.png"></a>
                </div>
            </div>
            <div class="col-md-9 col-sm-12 text-left text-left-xs text-left-sm">
                <div class="bar__module site-menu-sm">
                    <ul id="menu-menu-utama" class="menu-horizontal text-left">
                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="profil.php">Profil</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="tentang.php">Tentang</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="pelatihan.php">Pelatihan HKI</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-200 dropdown">
                            <span class="dropdown__trigger">Lainnya</span>
                            <div class="dropdown__container">
                                <div class="container">
                                    <div class="row">
                                        <div class="dropdown__content bg--blue col-md-2 col-sm-4">
                                            <ul class="menu-vertical">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="list_paten.php">Daftar Paten</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="peraturan_dan_uu.php">Peraturan dan UU</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="panduan_peraturan.php">Panduan HKI</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="text-right top-search"><a href="#" data-notification-link="search-box"><i class="stack-search"></i></a></div>
        </div>
    </nav>
</div>
<div class="notification pos-top pos-right search-box bg--white border--bottom" data-animation="from-top" data-notification-link="search-box">
    <form enctype="multipart/form-data" method="post" id="searchform" class="searchform" action="search_all.php" role="search">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <input type="search" name="s" value="" placeholder="Ketik yang Anda cari lalu enter..." />
            </div>
        </div>
    </form>
    <div class="notification-close-cross notification-close"></div>
</div>