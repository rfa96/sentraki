<script type='text/javascript' src='assets/js/isotope.min-ver=2.2.0.js'></script>
<script type='text/javascript' src='assets/js/smooth-scroll.min-ver=2.2.0.js'></script>
<script type='text/javascript' src='assets/js/scripts-ver=2.2.0.js'></script>
<script type='text/javascript' src='assets/js/wp-embed.min-ver=4.9.4.js'></script>
<script type="text/javascript" src="assets/js/bootstrap_337.js"></script>
<script src="assets/owlcarousel/dist/owl.carousel.min.js"></script>
<script>
    $(window).on('load', function () {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items: 1,
            loop: true,
            margin:0,
            autoplay: true,
            autoplayTimeout:3000,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut',
            autoHeight: true,
            nav: false,
            dots: false
        });
        owl.trigger('play.owl.autoplay', [3000]);
    });
</script>