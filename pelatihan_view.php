<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/17/18
 * Time: 01:18 AM
 */
    include "koneksi.php";
    $id_pelatihan = $_GET['id_pelatihan'];
    $sql_pelatihan = $conn->query("SELECT * FROM pelatihan WHERE id_pelatihan = ".$id_pelatihan);
    $row_pelatihan = $sql_pelatihan->fetch_array();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "head_tag.php";?>
        <title>View Pelatihan - HAKI Polinema</title>
    </head>
    <body class="page-template-default page page-id-53 page-child parent-pageid-5" data-smooth-scroll-offset="80">
        <?php include "navbar.php";?>

        <div class="main-container">
            <section class="text-center space--xs bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                            <h1><?= $row_pelatihan[1]?></h1>
                            <p><?= "Diunggah pada: ".date("d-M-Y", strtotime($row_pelatihan[3]))." Jam ".$row_pelatihan[4]?></p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <?= $row_pelatihan[2]?>
                        </div>
                    </div>
                </div>
            </section>
            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
    </body>
</html>