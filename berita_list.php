<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/17/18
 * Time: 08:44 AM
 */
    include "koneksi.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "head_tag.php"?>
        <title>List Semua Berita - HAKI Polinema</title>
    </head>
    <body class="page-template-default page page-id-53 page-child parent-pageid-5" data-smooth-scroll-offset="80">
        <?php include "navbar.php";?>

        <div class="main-container">
            <?php include "carousel.php";?>

            <section class="space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="text-center">ARSIP BERITA</h1>
                            <?php
                                if(isset($_GET['page']))
                                {
                                    $page = $_GET['page'];
                                }
                                else
                                {
                                    $page = 1;
                                }

                                if($page == '' || $page == 1)
                                {
                                    $page1 = 0;
                                }
                                else
                                {
                                    $page1 = ($page * 5) - 5;
                                }

                                $sql_berita = $conn->query("SELECT * FROM berita ORDER BY id_berita DESC LIMIT $page1, 5");

                                //Fetch
                                while($row_berita = $sql_berita->fetch_array())
                                {
                                    ?>
                                        <div class="card-2">
                                            <div class="card__body">
                                                <font size="5"><?= $row_berita[2]?></font><br>
                                                <a href="berita_view.php?id_berita=<?= $row_berita[0]?>">
                                                    Selengkapnya >>>
                                                </a>
                                            </div>
                                        </div><br>
                                    <?php
                                }

                                //Pagination
                                $sql_berita_all = $conn->query("SELECT * FROM berita");
                                $records = $sql_berita_all->num_rows;
                                $records_pages = $records / 5;
                                $records_pages = ceil($records_pages);
                                $prev = $page - 1;
                                $next = $page + 1;

                            ?>
                                <br>
                                <center>
                                    <nav aria-label="Page navigation">
                                        <ul class="pagination pagination-lg">
                                            <?php
                                                if($page >= 5)
                                                {
                                                    ?>
                                                        <li>
                                                            <a href="?page=1" aria-label="Previous">
                                                                <span aria-hidden="true">First</span>
                                                            </a>
                                                        </li>
                                                    <?php
                                                }

                                                if($prev >= 1)
                                                {
                                                    ?>
                                                    <li>
                                                        <a href="?page=<?= $prev?>" aria-label="Previous">
                                                            <span aria-hidden="true">&laquo;</span>
                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                        <li class="disabled">
                                                            <a aria-label="Previous">
                                                                <span aria-hidden="true">&laquo;</span>
                                                            </a>
                                                        </li>
                                                    <?php
                                                }

                                                if($records_pages >= 0)
                                                {
                                                    for($r = 1; $r <= $records_pages; $r++)
                                                    {
                                                        $active = $r == $page ? 'class="active"' : '';
                                                        ?>
                                                            <li><a href="?page=<?= $r?>"><?= $r?></a></li>
                                                        <?php
                                                    }
                                                }

                                                if($next <= $records_pages && $records_pages >= 2)
                                                {
                                                    ?>
                                                        <li>
                                                            <a href="?page=<?= $next;?>" aria-label="Next">
                                                                <span aria-hidden="true">&raquo;</span>
                                                            </a>
                                                        </li>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                        <li class="disabled">
                                                            <a aria-label="Next">
                                                                <span aria-hidden="true">&raquo;</span>
                                                            </a>
                                                        </li>
                                                    <?php
                                                }

                                                if($page != $records_pages && $records_pages >= 5)
                                                {
                                                    ?>
                                                        <li>
                                                            <a href="?page=<?= $records_pages?>" aria-label="Next">
                                                                <span aria-hidden="true">Last</span>
                                                            </a>
                                                        </li>
                                                    <?php
                                                }
                                            ?>
                                        </ul>
                                    </nav>
                                </center>
                        </div>
                    </div>
                </div>
            </section>

            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
    </body>
</html>