<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/12/18
 * Time: 21:31 PM
 */
    include "koneksi.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "head_tag.php";?>
        <title>Peraturan dan UU - HAKI Polinema</title>
        <link rel="stylesheet" type="text/css" href="assets/css/datatables_bs4.min.css"/>
        <script type="text/javascript" src="assets/js/datatables_bs4.min.js"></script>
    </head>
    <body class="page-template-default page page-id-53 page-child parent-pageid-5" data-smooth-scroll-offset="80">
        <?php include "navbar.php";?>

        <div class="main-container">
            <?php include "carousel.php";?>

            <section class="space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="text-center">Peraturan dan Undang-Undang</h1>
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Cipta</a>
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <table class="reg-table display" id="tabel_peraturan_cipta" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama File</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>UU No. 28 Tahun 2014 Tentang HAK CIPTA</td>
                                                        <td><a href="assets/wp-contents/cipta/UU%20Hak%20Cipta%2028-2014.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>PP RI NO. 01 TAHUN 1989</td>
                                                        <td><a href="assets/wp-contents/cipta/PP1-1989PerbanyakanCiptaan.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>PP RI NO. 29 TAHUN 2004</td>
                                                        <td><a href="assets/wp-contents/cipta/Peraturan-Pemerintah-tahun-2004-029-04.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Merek</a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="reg-table display" id="tabel_peraturan_merek" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama File</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>UU RI NO. 15 TAHUN 2001</td>
                                                        <td><a href="assets/wp-contents/merek/uu_no_15_th_2001.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>PP RI NO. 23 TAHUN 1993</td>
                                                        <td><a href="assets/wp-contents/merek/pp_23_1993_tata_cara_permintaan_pendaftaran_merek.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>PP RI NO. 32 TAHUN 1995</td>
                                                        <td><a href="assets/wp-contents/merek/pp_32_1995_ttg_kbm.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>PP RI NO. 07 TAHUN 2005</td>
                                                        <td><a href="assets/wp-contents/merek/Peraturan-Pemerintah-tahun-2005-007-05.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>PP RI NO. 20 TAHUN 2005</td>
                                                        <td><a href="assets/wp-contents/merek/PP_No_20Th2005ttgAlihTeknologi.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>PP RI NO. 51 TAHUN 2007</td>
                                                        <td><a href="assets/wp-contents/merek/PP-Nomor-51-Tahun-2007-Indikasi-Geografis.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7</td>
                                                        <td>Naskah Akademik RUU tentang Merek 2015</td>
                                                        <td><a href="assets/wp-contents/merek/Naskah%20Akademik%20RUU%20tentang%20Merek%202015.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>8</td>
                                                        <td>RUU MEREK 2015</td>
                                                        <td><a href="assets/wp-contents/merek/RUU%20MEREK%202015.doc" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9</td>
                                                        <td>UU No. 20 Tahun 2016 tentang Merek dan Indikasi Geografis</td>
                                                        <td><a href="assets/wp-contents/merek/UU%20no%2020%20tahun%202016%20tentang%20Merek%20dan%20IG.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Hak Paten</a>
                                        </h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="reg-table display" id="tabel_peraturan_paten" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama File</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>UU No. 14 Tahun 2001 Tentang PATEN</td>
                                                        <td><a href="assets/wp-contents/paten/UU14-2001Paten.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>PP No. 31 Tahun 1995 Tentang Komisi Banding Paten</td>
                                                        <td><a href="assets/wp-contents/paten/Peraturan-Pemerintah-tahun-1995-031-95(1).pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>PP No. 40 Tahun 2005 Ttg Susunan Organisasi, Tugas dan Fungsi Komisi Banding Paten</td>
                                                        <td><a href="assets/wp-contents/paten/pp_40_2005.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>PPI No. 27 Tahun 2004 Ttg Tata Cara Pelaksanaan Paten oleh Pemerintah</td>
                                                        <td><a href="assets/wp-contents/paten/PP27-2004PatenPemerintah.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>KEPRES No. 83 TAHUN 2004 Pelaksanaan Paten Oleh Pemerintah Terhadap obat-obat Anti Retroviral</td>
                                                        <td><a href="assets/wp-contents/paten/Keputusan-Presiden-tahun-2004-083-04.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>UU NO. 13 Tahun 2016 Tentang PATEN</td>
                                                        <td><a href="assets/wp-contents/paten/UU%20No_%2013%202016.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Desain Industri</a>
                                        </h4>
                                    </div>
                                    <div id="collapse4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="reg-table display" id="tabel_peraturan_desain_industri" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama File</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>UU RI NO. 31 TAHUN 2000</td>
                                                        <td><a href="assets/wp-contents/desain_industri/UU31-2000DesainIndustri.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>PP RI NO 01 TAHUN 2005 tentang pelaksanaan UU NO. 31 TAHUN 2000</td>
                                                        <td><a href="assets/wp-contents/desain_industri/PERPU_NO_1_TH_2005.doc" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Rahasia Dagang</a>
                                        </h4>
                                    </div>
                                    <div id="collapse5" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="table table-bordered" id="tabel_peraturan_rahasia_dagang" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama File</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>UU RI NO. 30 TAHUN 2000</td>
                                                        <td><a href="assets/wp-contents/rahasia_dagang/UU_RD_30_2000.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">Desain Tata Letak Sirkuit Terpadu</a>
                                        </h4>
                                    </div>
                                    <div id="collapse6" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="reg-table display" id="tabel_peraturan_sirkuit_terpadu" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama File</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>UU RI NO. 32 TAHUN 2000</td>
                                                        <td><a href="assets/wp-contents/tata_letak_sirkuit_terpadu/UU_Nomor%2032%20Tahun%202000%20DTLST(1).pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>PP RI NO. 09 TAHUN 2006</td>
                                                        <td><a href="assets/wp-contents/tata_letak_sirkuit_terpadu/Peraturan-Pemerintah-tahun-2006-009-06.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">Lain-Lain</a>
                                        </h4>
                                    </div>
                                    <div id="collapse7" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <table class="reg-table display" id="tabel_peraturan_lainnya" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama File</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>PP RI NO. 14 TAHUN 1986</td>
                                                        <td><a href="assets/wp-contents/lain_lain/pp_14_1986_ttg_dewanhc.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>PP RI NO. 07 TAHUN 1989</td>
                                                        <td><a href="assets/wp-contents/lain_lain/89pp007%20pp%20ri%20no%207%20tahun%201989.doc" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>PP RI NO 32 TAHUN 1991</td>
                                                        <td><a href="assets/wp-contents/lain_lain/pp%20no%2032%20th%201991.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>UU RI NO. 05 TAHUN 1999</td>
                                                        <td><a href="assets/wp-contents/lain_lain/UU%20RI%20no%205%20th%201999.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>UU RI NO. 18 TAHUN 2002</td>
                                                        <td><a href="assets/wp-contents/lain_lain/UU_No__18_Th_2002_danPenjelasannya.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>PP RI NO. 02 TAHUN 2005</td>
                                                        <td><a href="assets/wp-contents/lain_lain/PP_No_2_th_2005.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7</td>
                                                        <td>PP RI NO. 20 TAHUN 2005</td>
                                                        <td><a href="assets/wp-contents/lain_lain/PP_No_20Th2005ttgAlihTeknologi(1).pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>8</td>
                                                        <td>PP RI NO. 38 TAHUN 2009</td>
                                                        <td><a href="assets/wp-contents/lain_lain/pp_38_2009(1).pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9</td>
                                                        <td>UU RI NO. 29 TAHUN 2000</td>
                                                        <td>
                                                            <a href="assets/wp-contents/lain_lain/UU_29_2000_PVT(1).pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>10</td>
                                                        <td>PP RI NO. 13 TAHUN 2004</td>
                                                        <td><a href="assets/wp-contents/lain_lain/PP-13-04.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>11</td>
                                                        <td>PP RI NO. 14 TAHUN 2004</td>
                                                        <td><a href="assets/wp-contents/lain_lain/PP-14-04.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>12</td>
                                                        <td>UU RI NO. 08 TAHUN 1999</td>
                                                        <td><a href="assets/wp-contents/lain_lain/uu%20ri%20no%208%20tahun%201999.pdf" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> VIEW</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
        <script>
            $(document).ready(function () {
                $('#tabel_peraturan_cipta').DataTable();
                $('#tabel_peraturan_merek').DataTable();
                $('#tabel_peraturan_paten').DataTable();
                $('#tabel_peraturan_desain_industri').DataTable();
                $('#tabel_peraturan_rahasia_dagang').DataTable();
                $('#tabel_peraturan_sirkuit_terpadu').DataTable();
                $('#tabel_peraturan_lainnya').DataTable();
            });
        </script>
    </body>
</html>