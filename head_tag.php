<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="assets/img/favicon.ico">
<link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
<meta name="apple-mobile-web-app-title" content="UPT P2M Polinema">
<meta name="application-name" content="UPT P2M Polinema">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#002d72">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-2070620-33');
</script>

<script type="text/javascript">
    window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/p2m.polinema.ac.id\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.4"}};
    !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
</script>

<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
</style>

<link rel='stylesheet' id='fontawesome-css'  href='assets/inc/classes/custom-controls/css/font-awesome.min-ver=4.7.css' type='text/css' media='all' />
<!--<link rel='stylesheet' id='wpex-bootstrap-css'  href='assets/css/bootstrap-ver=2.2.0.css' type='text/css' media='all' />-->
<link rel="stylesheet" id="wpex-bootstrap-css" href="assets/css/bootstrap_337.css" media="all"/>
<link rel='stylesheet' id='wpex-font-main-css'  href='assets/css/font-main-ver=2.2.0.css' type='text/css' media='all' />
<link rel='stylesheet' id='wpex-interface-css'  href='assets/css/stack-interface-ver=2.2.0.css' type='text/css' media='all' />
<link rel='stylesheet' id='wpex-style-css'  href='assets/style-ver=2.2.0.css' type='text/css' media='all' />
<link rel='stylesheet' id='wpex-custom-styles-css'  href='assets/css/custom-ver=2.2.0.css' type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:400,400i,700' type='text/css' media='all' />
<link rel='stylesheet' id='material-icons-css'  href='http://fonts.googleapis.com/icon?family=Material+Icons' type='text/css' media='all' />
<link rel="stylesheet" href="assets/owlcarousel/dist/assets/owl.carousel.css"/>
<link rel="stylesheet" href="assets/owlcarousel/dist/assets/owl.theme.default.css"/>
<link rel="stylesheet" href="assets/css/animate.css"/>
<script type="text/javascript" src="assets/js/jquery_331.js"></script>
<script type="text/javascript" src="assets/js/bootstrap_337.js"></script>
<script type='text/javascript' src='assets/js/jquery-migrate.min-ver=1.4.1.js'></script>
<link rel="stylesheet" type="text/css" href="assets/css/datatables.min.css"/>
<script type="text/javascript" src="assets/js/pdfmake.min.js"></script>
<script type="text/javascript" src="assets/js/vfs_fonts.js"></script>
<script type="text/javascript" src="assets/js/datatables.min.js"></script>

