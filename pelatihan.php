<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/14/18
 * Time: 10:41 AM
 */
    include "koneksi.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "head_tag.php";?>
        <title>Pelatihan HAKI Polinema</title>
    </head>
    <body class="page-template-default page page-id-53 page-child parent-pageid-5" data-smooth-scroll-offset="80">
        <?php include "navbar.php";?>

        <div class="main-container">
            <?php include "carousel.php";?>

            <section class="space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="text-center">Pelatihan HAKI Polinema</h1>
                            <?php
                                $sql_pelatihan = $conn->query("SELECT * FROM pelatihan");
                                if($sql_pelatihan->num_rows > 0)
                                {
                                    while($row_pelatihan = $sql_pelatihan->fetch_array())
                                    {
                                        ?>
                                            <div class="card-2">
                                                <div class="card__body">
                                                    <font size="5"><?= $row_pelatihan[1]?></font><br>
                                                    <a href="pelatihan_view.php?id_pelatihan=<?= $row_pelatihan[0]?>">
                                                        Selengkapnya >>>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                        <div class="card-2">
                                            <div class="card__body">
                                                No data available on this page!
                                            </div>
                                        </div><br>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </section>

            <?php include "footer.php";?>
        </div>
        <?php include "assets_js.php";?>
    </body>
</html>