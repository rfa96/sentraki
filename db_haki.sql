-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~xenial.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 26, 2018 at 02:36 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_haki`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `id_agenda` int(11) NOT NULL,
  `nama_agenda` text NOT NULL,
  `konten_agenda` text NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`id_agenda`, `nama_agenda`, `konten_agenda`, `tanggal`, `jam`, `id_user`) VALUES
(1, 'Bimbingan Teknis Hak Kekayaan Intelektual (HKI) Bagi Dosen/Pengajar Di Lingkungan UMM', '<p style=\"text-align: justify;\">BimBingan Teknis Hak Kekayaan Intelektual (HKI) Bagi Dosen/Pengajar Di Lingkungan Universitas Muhammadiyah Malang oleh Pejabat Direktorat Jenderal Hak Kekayaan Intelaktual</p>\r\n<p style=\"text-align: justify;\">Dilaksanakan pada:</p>\r\n<p style=\"text-align: justify;\">Hari/Tanggal :&nbsp; Rabu,&nbsp; 23 November 2011</p>\r\n<p style=\"text-align: justify;\">Tempat : Ruang Sidang Senat UMM</p>\r\n<p style=\"text-align: justify;\">Acara : BimBingan Teknis Hak Kekayaan Intelektual (HKI) Bagi Dosen/Pengajar Di Lingkungan Universitas Muhammadiyah Malang</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>', '2018-09-27', '14:19:10', 1),
(2, 'Penandatanganan MOU Sentra HKI UMM - Kemenkumham', '<p>Kerjasama tentang sosialisasi, pemanfaatan, pelatihan, kursus tentang hak kekayaan intelektual. Penandatanganan MOU dilakukan oleh Rektor UMM di kantor <strong>Kementerian Hukum dan Ham RI di Jakarta</strong></p>', '2018-10-04', '13:10:58', 1),
(3, 'Kunjungan Tamu dari UPN Surabaya & Universitas Hangtuah Surabaya (22 Oktober 2012)', '<p>Kunjungan Tamu dri UPN Surabaya &amp; Universitas Hangtua Surabaya (22 Oktober 2012)</p>', '2018-10-21', '19:10:57', 1),
(8, 'Polinema Sukses Miliki Enam TUK', '<p>Politeknik Negeri Malang (Polinema) meraih penghargaan sebagai lembaga pelayanan publik bersertifikat baik. Penghargaan ini diberikan Menteri Pendidikan Nasional (Mendiknas) M Nuh pada malam 17 Agustus yang juga bertepatan dengan malam 17 Ramadan kemarin. Polinema adalah satu-satunya perguruan tinggi jenjang vokasi yang berhasil mendapatkan piagam ini.<br />&rdquo;Ada tujuh lembaga lain yang mendapat piagam ini, dan hanya Polinema yang mewakili jenjang vokasi,&rdquo; ungkap Direktur Polinema, Ir Tundung Subali Patma MT kepada Malang Post.<br />Tundung menuturkan Polinema sebelumnya dipercaya mewakili jenjang vokasi untuk mengikuti lomba ini. Selain dari jenjang vokasi, penilaian juga dilakukan untuk lembaga perguruan tinggi dan juga lembaga pelatihan. Penilaian dilakukan langsung oleh tim dari pusat tanpa persiapan apapun dari pihak Polinema. Sebab tim yang datang tanpa pemberitahuan langsung melakukan penilaian kepada stakeholder pengguna layanan. Di antaranya kepada orang tua siswa, mahasiswa dan juga dosen.<br />&rdquo;Tidak ada persiapan apapun dan memang tidak mungkin disiapkan, karena tim penilai datang tanpa memberitahu apapun pada kita,&rdquo; jelasnya.<br />Dari tujuh lembaga yang dinilai, enam lembaga meraih predikat baik termasuk Polinema. Sementara satu lembaga yaitu lembaga pelatihan dari Bandung meraih nilai sangat baik. Perbedaan nilai ini menurut Tundung wajar terjadi karena seharusnya perguruan tinggi tidak bisa disamakan dengan lembaga pelatihan.<br />&rdquo;Saya sudah mengusulkan agar pada penilaian selanjutnya bisa dibedakan antara lembaga pelatihan dengan perguruan tinggi,&rdquo; tukasnya.<br />Hanya saja dari penilaian ini ada beberapa hal yang masih harus ditingkatkan. Diantaranya adalah kemudahan layanan sistem informasi akademik bagi stakeholder. Contohnya saja untuk mengetahui nilai akademik mahasiswa bisa disediakan layanan online yang biasa diakses dimana pun. Sehingga mahasiswa tidak perlu datang ke kampus cukup mengakses darimana saja yang ada layanan internetnya.<br />&rdquo;Tidak sulit bagi Polinema untuk menyiapkan layanan ini, sebab SDM maupun sarananya sudah siap,&rdquo; pungkasnya. (oci/eno)</p>', '2018-10-22', '19:10:01', 1),
(9, 'Bertekad Mengulang Sukses PIMNAS, Polinema Giatkan PKM Untuk Mendongkrak Prestasi Mahasiswa', '<p>Ribuan maba Polinema memadati Gedung Aula Pertamina Polinema, Rabu (23/10) lalu. Mereka tampak antusias mengikuti Sosialisasi Program Kreativitas Mahasiswa (PKM) yang digelar Bidang III Polinema, dan dikomandani Staf Ahli Bidang III sebagai Ketua Panitia, Nanak Zakaria, ST., MT.</p>\r\n<p>Sosialisasi secara resmi dibuka oleh Pembantu Direktur III, Drs. Halid Hasan, M.Strat.HRM. &ldquo;Sudah saatnya mahasiswa berkreasi untuk mengukir prestasi yang dapat membanggakan dirinya, orang tua dan lembaga,&rdquo; ungkap Halid dalam sambutannya.</p>\r\n<p>Kegiatan Sosialisasi PKM merupakan kali pertama yang digelar oleh Bidang III, dengan harapan akan ada peningkatan proposal PKM yang masuk ke Dikti, baik dari segi kuantitas maupun kualitas. &ldquo;Tahun 2012 yang lalu, tanpa diadakan sosialisasi semacam ini saja sudah terkumpul ratusan proposal. Yang berhasil lolos didanai Dikti ada 21 proposal, sedangkan yang sangat membanggakan, ada satu judul proposal karya PKM yang berhasil lolos dalam ajang PIMNAS,&rdquo; tambah Halid &nbsp;yang bulan Desember nanti akan berpindah tugas menjadi Pembantu Direktur II ini.</p>\r\n<p>Sesi pertama sosialisasi ini hadirkan Bambang Suryanto, S.Pd., M.Pd, sebagai moderator dan Dr. Moechammad Sarosa, Dipl. Ing., MT sebagai pembicara tunggal. Pak Doktor Osa, panggilan akrab Dr. Moechammad Sarosa, Dipl. Ing., MT, menyampaikan bahwa ada beberapa hal yang dapat memotivasi mahasiswa untuk berkiprah di program ini, diantaranya adalah agar mahasiswa dapat memperoleh kesempatan untuk jalan-jalan ke luar negeri secara gratis. Hal ini diuangkapkan juga karena pengalaman pribadinya yang tidak dapat menyelesaikan pendidikannya di Program Diploma Politeknik Universitas Brawijaya karena harus berangkat ke Perancis untuk menempuh pendidikan&nbsp;<em>Diplome&nbsp;</em><em>Ingenieur</em>&nbsp;di sana. &ldquo;Itu salah satu yang dapat memotivasi anda, selain alasan lain yang lebih penting masalah akademis, saya harapkan anda harus kreatif untuk menciptakan ide-ide cemerlang&rdquo;, kata Pak Doktor, yang menyelesaikan program Master dan Doktornya di Institut Teknologi Bandung itu.</p>\r\n<p>Sesi kedua acara sosialisasi&nbsp; ini dimoderatori oleh Galuh Kartiko, SH., M.Hum, dan menghadirkan pembicara Dr. Eng. Anggit Murdani, ST., M.Eng., yang akan menyampaikan materi yang sama.</p>\r\n<p>Dr. Anggit menyampaikan bahwa motivasi bagi mahasiswa sangatlah penting, terutama mahasiswa baru. &ldquo;Motivasi harus diberikan agar mahasiswa terus berkreasi dan berkreasi untuk memunculkan inovasi,&rdquo; papar Anggit. &ldquo;Bangsa yang punya inovasilah yang dapat menguasai dunia, dan adik adik mahasiswalah pemilik masa depan bangsa ini,&rdquo; tambah Pak Doktor yang menyelesaikan S2 dan S3 Teknik Mesin di University of the Ryukyus Jepang tahun 2009 silam ini.</p>\r\n<p>Yang tak kalah menarik, di sisi depan aula terbentang spanduk yang dijadikan cambuk bagi peserta sosialisasi, bertuliskan: &ldquo;DUKUNG GERAKAN POLINEMA BERKARYA MELALUI PROGRAM KREATIVITAS MAHASISWA&rdquo;.</p>\r\n<p>Harapannya ke depan, semoga Pembantu Direktur Bidang III yang baru akan menyambut estafet kepemimpinan ini dengan baik agar kegiatan sosialisasi PKM ini akan selalu di gelar untuk membangkitkan gairah berkreasi&hellip; kami tunggu kiprahnya Pak PD III yang baru&hellip;&nbsp;<strong>(jok/yoe)</strong></p>', '2018-10-22', '19:10:00', 1),
(10, 'UPT Bahasa Polinema Gelar â€œMalang State Polytechnic English Competition (MSPEC)â€', '<p>UPT Bahasa Politeknik Negeri Malang pada Sabtu 19 Oktober 2013 lalu&nbsp;menyelenggarakan&nbsp;<em><strong>Malang State Polytechnic English Competition (MSPEC)</strong></em>. Lomba yang&nbsp;diadakan untuk pertama kalinya ini mengambil tema&nbsp;<em><strong>&ldquo;The Effect of Globalization on Technology&nbsp;and Business&rdquo;</strong></em>&nbsp;ini diikuti sekitar 90 orang peserta dari semua program studi dan jurusan di&nbsp;Politeknik Negeri Malang. Lomba ini meliputi 3 kategori yaitu&nbsp;<em>Speech Contest, News Reading,&nbsp;</em>dan&nbsp;<em>Story Telling</em>. Pemenang dari lomba ini mendapatkan hadiah berupa piala dan uang tunai&nbsp;serta nantinya akan diikutkan di lomba tingkat nasional, yaitu National Polytechnic English&nbsp;Competition.</p>\r\n<p>Adapun pemenang masing-masing kategori adalah sebagai berikut:</p>\r\n<p><strong>Speech Contest:</strong></p>\r\n<ol>\r\n<li>Yanuar (Teknik Mesin)</li>\r\n<li>Fauzia Akbar (Teknik Sipil)</li>\r\n<li>Ella Safitri (Manajemen Informatika)</li>\r\n</ol>\r\n<p><strong>News Reading:</strong></p>\r\n<ol>\r\n<li>Sonia (Administrasi Bisnis)</li>\r\n<li>Fachtur Rozi (Administrasi Bisnis)</li>\r\n<li>Gita Sukma Devyana (Teknik Telekomunikasi)</li>\r\n</ol>\r\n<p><strong>Story Telling:</strong></p>\r\n<ol>\r\n<li>Azkia Nury Fariza (Teknik Informatika)</li>\r\n<li>Hanna Sarah Ayuningrum (Akuntansi)</li>\r\n<li>Harley Oktavienta (Teknik Telekomunikasi)</li>\r\n</ol>', '2018-10-22', '19:10:58', 1),
(11, 'Program Kreatifitas Mahasiswa 2013', '<p>PKM&nbsp; merupakan&nbsp; salah&nbsp; satu&nbsp; upaya&nbsp; yang&nbsp; dilakukan oleh&nbsp; Direktorat&nbsp; Penelitian&nbsp; dan&nbsp; Pengabdian kepada&nbsp; Masyarakat&nbsp; (Ditlitabmas)&nbsp; Ditjen&nbsp; Dikti&nbsp; untuk&nbsp; meningkatkan&nbsp; mutu&nbsp; peserta&nbsp; didik&nbsp; (mahasiswa) di perguruan tinggi agar kelak dapat menjadi anggota masyarakat yang memiliki&nbsp; kemampuan&nbsp; akademis&nbsp; dan/atau&nbsp; profesional&nbsp; yang&nbsp; dapat&nbsp; menerapkan,&nbsp; mengembangkan&nbsp; dan&nbsp; meyebarluaskan&nbsp; ilmu&nbsp; pengetahuan,&nbsp; teknologi&nbsp; dan/atau&nbsp; kesenian&nbsp; serta&nbsp; memperkaya&nbsp; budaya nasional.&nbsp; PKM&nbsp; dilaksanakan&nbsp; pertama&nbsp; kali&nbsp; pada&nbsp; tahun&nbsp; 2001,&nbsp; yaitu&nbsp; setelah&nbsp; dilaksanakannya&nbsp; program restrukturisasi di lingkungan Ditjen Dikti. Kegiatan pendidikan, penelitian dan pengabdian kepada masyarakat yang selama ini sarat dengan partisipasi aktif mahasiswa, diintegrasikan ke dalam satu wahana,yaitu PKM.</p>\r\n<p>Pada awalnya, dikenal lima jenis kegiatan yang ditawarkan dalam PKM, yaitu PKM-Penelitian (PKM-P),&nbsp; PKM-Kewirausahaan&nbsp; (PKM-K),&nbsp; PKM-Pengabdian&nbsp; kepada&nbsp; Masyarakat&nbsp; (PKM-M), PKM-Penerapan&nbsp; Teknologi&nbsp; (PKM-T)&nbsp; dan&nbsp; PKM-Penulisan&nbsp; Ilmiah&nbsp; (PKM-I).&nbsp; Sejak&nbsp; Januari&nbsp; 2009, Ditlitabmasmengelola&nbsp; 6&nbsp; (enam)&nbsp; PKM.&nbsp; Kompetisi&nbsp; Karya&nbsp; Tulis&nbsp; Mahasis&not;wa&nbsp; (KKTM)yang&nbsp; semula&nbsp; menjadi&nbsp; tugas&nbsp; Direktorat&nbsp; Akademik&nbsp; dalam&nbsp; pengelolaannya,&nbsp; dilimpahkan&nbsp; kepada&nbsp; Ditlitabmas.&nbsp; Karena&nbsp; sifatnya&nbsp; yang&nbsp; identik&nbsp; dengan&nbsp; PKM-I,&nbsp; KKTM&nbsp; selanjutnya&nbsp; dikelola&nbsp; bersama-sama&nbsp; PKM-I&nbsp; dalam PKM-Karya Tulis (PKM-KT). Dengan demikian, di dalam PKM-KT terkandung dua program&nbsp; penulisan, yaitu: PKM-Artikel Ilmiah (PKM-AI) dan PKM-Gagasan Tertulis (PKM-GT). PKM-I atau&nbsp; selanjutnya disebut PKM-AI yang merupakan artikel hasil kegiatan, tidak lagi ditampilkan dalam&nbsp; PIMNAS, namun dimuarakan pada e-journal. Sedangkan PKM-GT yang berpeluang didiskusi&not;kan dalam forum terbuka, diposisikan sebagai pengganti PKM-AI di PIMNAS.</p>\r\n<p>Untuk pengusulan dan pendaftaran Program Kreatifitas Mahasiswa Tahun 2013 bagi mahasiswa Politeknik negeri Malang batas maksimal pada tanggal 31 Oktober 2013 di <a href=\"http://simlitabmas.dikti.go.id/\">http://simlitabmas.dikti.go.id/</a></p>\r\n<p>&nbsp;</p>', '2018-10-22', '19:10:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `judul` text,
  `konten_berita` text,
  `gambar` varchar(50) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jam` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `id_user`, `judul`, `konten_berita`, `gambar`, `tanggal`, `jam`) VALUES
(1, 1, 'Lomba Gagasan & Rancangan Kreatif (LoGRaK) Nasional 2018', '<p><strong>Lomba Gagasan &amp; Rancangan Kreatif (LoGRaK)</strong> adalah sebuah lomba perancangan multi disiplin bidang Teknik Elektro yang diselenggarakan rutin setiap tahun oleh Jurusan Teknik Elektro Politeknik Negeri Malang (Polinema). Kegiatan lomba ini bertujuan  untuk memberikan kesempatan bagi mahasiswa/i seluruh Indonesia untuk menyalurkan kreatifitasnya dan inovasinya dalam sebuah gagasan yang bertema “<em>Internet of Things</em> (IoT) dan <em>Renewable Energy Technology</em> dalam Era Revolusi Industri 4.0”. Tema lomba terbagi dalam 2 (dua) kategori, yaitu:</p>\r\n<ol>\r\n<li><strong><em>Renewable Energy Technology</em></strong></li>\r\n<li><strong><em>Internet of Things</em></strong><strong> (IoT) </strong></li>\r\n</ol>\r\n<p><strong>Persyaratan</strong></p>\r\n<ol>\r\n<li>Peserta LoGRaK adalah kelompok mahasiswa aktif program pendidikan Diploma 3 atau Diploma 4 atau Sarjana Jurusan <strong>Teknik Elektro dan Teknologi Informasi/Teknik Informatika.</strong></li>\r\n<li>Kelompok mahasiswa pengusul terdiri dari 3 &#8211; 4 orang.</li>\r\n<li>Seorang mahasiswa hanya diperkenankan masuk ke dalam 1 (satu) kelompok pengusul LoGRaK.</li>\r\n<li>Peserta menuangkan gagasannya ke dalam bentuk karya tulis.</li>\r\n<li>Karya tulis yang dikirimkan belum pernah mendapatkan predikat juara diajang perlombaan</li>\r\n<li>Peserta yang dinyatakan sebagai finalis akan diundang untuk mempresentasi gagasan karya tulis yang dibuat dan mengumpulkan poster dalam bentuk X Banner</li>\r\n<li>Biaya pendaftaran gratis</li>\r\n<li>Pendaftaran secara online melalui <strong>http://bit.ly/PENDAFTARANLOGRAKNASIONAL</strong></li>\r\n</ol>\r\n<p><strong>Jadwal Kegiatan  </strong></p>\r\n<ul>\r\n<li>Batas Penerimaan Karya: 15-26 Oktober 2018</li>\r\n<li>Seleksi Substansi Karya Tulis: 27-31Oktober 2018</li>\r\n<li>Pengumuman Finalis: 1 November 2018</li>\r\n<li>Presentasi: Kamis, 15 November  2018</li>\r\n</ul>\r\n<p><strong>Tempat</strong></p>\r\n<p>Gedung AH  R.Audit 1<br />\r\nPoliteknik Negeri Malang<br />\r\nJl. Soekarno Hatta No. 9 Malang, Jawa Timur 65141</p>\r\n<p><strong>Hadiah Per Kategori</strong></p>\r\n<ol>\r\n<li>Juara 1: Trophy + Setifikat + Uang Tunai Rp 2.000.000</li>\r\n<li>Juara 2 : Trophy + Setifikat + Uang Tunai Rp500.000</li>\r\n<li>Juara 3: Trophy + Setifikat + Uang Tunai Rp 75000</li>\r\n<li>Juara Poster semua kategori: Sertifikat + Uang Tunai Rp 500.000</li>\r\n</ol>\r\n<p><strong>Tata C</strong><strong>ara </strong><strong>P</strong><strong>endaftaran</strong></p>\r\n<p>Pendaftaraan lomba lograk hanya menggunakan cara on line, dengan langkah-langkah pendafatarn sebagai berikut:</p>\r\n<ul>\r\n<li>Buka <strong>http://bit.ly/PENDAFTARANLOGRAKNASIONAL<br />\r\n</strong>Isi form pendaftaran dengan lengkap</li>\r\n</ul>\r\n<ul>\r\n<li>Unggah semua persyaratan serta karya tulis dalam format PDF :<br />\r\n<strong>Nama ketua_Nama Tim_Nama Kampus</strong></li>\r\n</ul>\r\n<p><strong> Informasi </strong></p>\r\n<p>Jevin Febriantoro    : 085854715942</p>\r\n<p>Ferdian Ronilaya    : 0857302607640</p>\r\n<p>Lis Diana Mustafa  : 085102103006</p>\r\n<p><strong>Catatan:</strong> Makalah yang masuk tidak dikembalikan.</p>\r\n<h2><strong>DOWNLOAD CONTOH PANDUAN PENULISAN DISINI : <a href=\"http://www.polinema.ac.id/wp-content/uploads/2018/09/panduan-penulisan-lograk-2018.doc\">DOWNLOAD</a></strong></h2>', '', '2018-09-20', '10:15:06'),
(2, 1, 'Politeknik Negeri Malang Berkolaborasi Dalam Penelitian Dengan Litbang PLN Pusat dan Divisi IT', '<p><strong>Politeknik Negeri Malang</strong>&nbsp;berkolaborasi dalam penelitian dengan Litbang PLN Pusat dan Divisi IT. Kunjungan staff akademik yang dipimpin oleh Dr. Luchis Rubianto pada tanggal 16-17 April 2018 melakukan kunjungan dan diskusi dengan General Manager LitBang PLN Bapak E. Haryadi, serta Kadiv IT PL Bapak Agus Setiawan. Pada tahun 2018 ini, Polinema menambah beberapa skim baru penelitian bagi dosen dalam upaya pemanfaatan hasil penelitian dosen yang dapat dimanfaatkan oleh industri, yaitu salah satunya skim penelitian kerjasama industri. Skim penelitian ini ditujukan untuk meningkatkan jumlah Hak Kekayaan Intelektual Polinema.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"../haki_polinema_2/assets/wp-contents/204183.png.jpg\" alt=\"\" width=\"637\" height=\"478\" /></p>\r\n<p style=\"text-align: center;\">Gambar 1: Dosen Polinema beserta Staff PLN</p>', 'cobacoba.jpg', '2018-10-06', '23:10:44'),
(4, 1, 'Program CSR BRI, Pemberian Ambulance ke Polinema', '<p style=\"text-align: justify;\">Kerjasama antara Politeknik Negeri Malang (Polinema) dengan Bank Rakyat Indonesia (BRI) terjalin sejak tahun 2015 dan hingga kini terus dikembangkan. Kamis (11/10/2018) kemarin, Polinema menerima bantuan CSR berupa 1 unit&nbsp;<em>ambulance</em>senilai Rp 409 juta dari BRI Soekarno Hatta yang diserahkan oleh Pimpinan BRI Wilayah Malang Eko Wahyudi kepada Direktur Polinema Drs Awan Setiawan, MM, di halaman Gedung Pusat Polinema.</p>\r\n<p style=\"text-align: justify;\">&ldquo;Kerjasama antara BRI &ndash; Polinema terjalin sejak lama. Sebelumnya, BRI pernah memberikan Laboratorium Perbankan kepada Polinema. Untuk kesekian kalinya, BRI kembali memberikan 1 unit ambulance baru. Sebelumnya, Polinema pernah menerima bantuan&nbsp;<em>ambulance</em>&nbsp;dari Ristekdikti namun&nbsp;<em>second</em>, sekarang kondisinya lebih baru dari BRI. Penggunaan&nbsp;<em>ambulance</em>&nbsp;ini tak hanya untuk sivitas akademika Polinema, namun juga digunakan oleh warga sekitar kampus yang membutuhkan,&rdquo; jelas Direktur Polinema dalam sambutannya.</p>\r\n<p style=\"text-align: justify;\">Selama ini, simpanan karyawan dan dosen Polinema paling banyak di BRI. Tak hanya simpanan, tetapi juga memanfaatkan pinjaman. Beberapa fasilitas penunjang dan infrastruktur Polinema juga diperoleh dari CSR bank-bank lain dan perusahaan, seperti tempat parkir, bantuan pembangunan masjid, laboratorium, dan lainnya. Diharapkan, kerjasama BRI tak hanya berhenti pada CSR barang dan infrastruktur, namun lebih ditekankan pada pemberdayaan SDM.</p>\r\n<p style=\"text-align: justify;\">Setiap tahun, wisudawan Polinema terserap kerja lebih dari 70 persen. Ini menunjukkan bahwasanya minat dunia usaha dan dunia industri cukup tinggi terhadap kompetensi lulusan Polinema. Sudah ada beberapa kelas kerjasama dengan perusahaan dan BUMN di Polinema, seperti PLN, GMF Aeroasia, Alfamart, Bukit Asam, dan lainnya. &ldquo;Kami harapkan, BRI dalam waktu dekat juga demikian, bersedia dalam Kelas Kerjasama dengan muatan kurikulum khusus dari BRI. Selain itu, ada beasiswa ikatan kerja untuk mahasiswa semester akhir,&rdquo; tambah Awan.</p>\r\n<p style=\"text-align: justify;\">Sementara itu, Pimpinan BRI Wilayah Malang Eko Wahyudi akan membicarakan lebih lanjut penawaran dari Polinema tentang Kelas Kerjasama BRI dan Beasiswa Ikatan Kerja. &ldquo;Semoga CSR&nbsp;<em>ambulance</em>&nbsp;ini lebih bermanfaat. Terkait Kelas Kerjasama dan beasiswa, nanti akan kami bahas itu lebih lanjut. Cukup bagus untuk peningkatan SDM. Kami juga mengharapkan adanya kerjasama lanjutan, yaitu fasilitas pembayaran SPP mahasiswa di BRI,&rdquo; jelas Eko Wahyudi.</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"../haki_polinema_2/assets/wp-contents/WhatsApp-Image-2018-10-11-at-14.52.15-1.jpeg\" alt=\"\" width=\"612\" height=\"408\" /></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 1:&nbsp;Jajaran Direksi Polinema bersama BRI</strong></p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p style=\"text-align: center;\"><img src=\"../haki_polinema_2/assets/wp-contents/IMG-20181011-WA0155.jpg\" alt=\"\" width=\"619\" height=\"292\" /></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 2:&nbsp;Penyerahan Simbolis Ambulance BRI kepada Polinema</strong></p>', 'IMG-20181011-WA0155.jpg', '2018-10-11', '18:10:32'),
(5, 1, 'Edukasi E-Commerse tentang Belanja Online oleh Dinas Koperasi dan Usaha Mikro Kota Malang', '<p style=\"text-align: justify;\">Kepemimpinan Kota Malang yang baru oleh Bapak Sutiaji, menargetkan Kota Malang selain sebagai Kota Pariwisata dan Pendidikan, namun juga sebagai Kota UMKM. Seiring revolusi industri 4.0, Pemerintah Kota Malang melalui sektor UMKM mulai berbenah diri dengan mengikuti perkembangan teknologi digital. Untuk melaksanakan hal tersebut, pemerintah perlu menggandeng stakeholders, BUMN, Perguruan Tinggi, dan lainnya. Salah satu bentuk yang akan direalisasikan adalah menjadikan Ex Ramayana menjadi Mall UMKM sebagai wadah promosi dan pemasaran UMKM.</p>\r\n<p style=\"text-align: justify;\">&ldquo;Tentunya butuh proses, seperti dalam acara yang diselenggarakan Kementerian Komunikasi dan Informatika yang bekerjasama dengan Dinas Koperasi dan UKM Kota Malang ini. Kehadiran pihak lain seperti Bukalapak, tentu akan memudahkan pola pemasaran produk UMKM ini. Selain itu, peran Perguruan Tinggi dalam edukasi manajemen,&rdquo; jelas Sutiaji, ketika membuka acara sosialisasi dan edukasi&nbsp;<em>e-commerce</em>&nbsp;dengan tajuk&nbsp;<em>Belanja dan Jualan Online: Murah, Cepat, dan Aman</em>, di Aula Pertamina Politeknik Negeri Malang, Kamis (11/10/2018) dengan 3 narasumber kompeten di bidangnya, diantaranya Pakar Ekonomi Universitas Brawijaya Dr. Dias Satria, Wakil Ketua Komite Ekonomi Kreatif Vicky Arief Herinadharma, dan Koordinator Ranger Komunitas Bukalapak Malang Andre Uno.</p>\r\n<p style=\"text-align: justify;\">Kepala Dinas Koperasi dan Usaha Mikro Kota Malang Dra. Tri Widyani P., M.Si, mengatakan, sinergitas pemerintah daerah, Perguruan Tinggi, dan komunitas pengusaha, untuk memberikan pendampingan UMKM menuju&nbsp;<em>high class</em>, dengan memberikan ruang promosi untuk UMKM melalui&nbsp;<em>networking</em>. Seperti halnya, Kementerian Kominfo dan Dinas Koperasi Kota Malang, menggandeng&nbsp;<em>marketplace</em>&nbsp;Bukalapak untuk cara pemasaran&nbsp;<em>online</em>, dan menggandeng Polinema melalui forum bisnisnya.</p>\r\n<p style=\"text-align: justify;\">Dari 300 peserta UMKM umum ini, nantinya akan dibimbing dan diberikan pelatihan oleh Bukalapak. Upaya ini untuk meminimalisir kerugian dari penipuan transaksi jual beli yang diunggah melalui medsos. Saat ini, lebih dari 100 kota yang memiliki beberapa komunitas, dengan anggota 3 juta lebih UMKM. Separuh diantaranya murni pelaku UMKM sebagai produsen. &ldquo;Nantinya, mereka akan dibuatkan akun di Bukalapak. Mereka akan dibimbing dalam kelas pelatihan. Kami akan diskusi dan sharing atas permasalahan yang dihadapi,&rdquo; jelas Ranger Komunitas Bukalapak Malang Asrul Tsani, mendampingi Andre Uno, Koordinator Ranger Komunitas Bukalapak Malang. (nitha/humas).</p>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: center;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"../haki_polinema_2/assets/wp-contents/WhatsApp-Image-2018-10-11-at-14.52.15-768x512.jpeg\" alt=\"\" width=\"768\" height=\"512\" /></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 1:&nbsp;Sosialisasi UMKM diikuti sekitar 300 peserta</strong></p>\r\n<p style=\"text-align: center;\"><strong><img src=\"../haki_polinema_2/assets/wp-contents/WhatsApp-Image-2018-10-11-at-14.50.13.jpeg\" alt=\"\" width=\"772\" height=\"514\" /></strong></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 2:&nbsp;Penguatan UMKM memerlukan sinergitas dengan berbagai pihak salah satunya pemerintahan dengan perguruan tinggi seperti Pemkot Malang dengan Polinema</strong></p>\r\n<p style=\"text-align: center;\"><strong><img src=\"../haki_polinema_2/assets/wp-contents/WhatsApp-Image-2018-10-11-at-14.51.11-768x512.jpeg\" alt=\"\" width=\"768\" height=\"512\" /></strong></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 3:&nbsp;Walikota Malang Bapak Sutiaji Memberikan sambutan dalam pembukaan Sosialisasi Belanja Online di Polinema</strong></p>', 'WhatsApp-Image-2018-10-11-at-14.50.13.jpeg', '2018-10-21', '12:10:23'),
(6, 1, 'MOU Dengan Universite de valenciennes Perancis', '<p style=\"text-align: justify;\">Polinema- Selasa siang (27/10) di ruang Teleconference KUI (kantor Urusan Luar Negeri) mendapat kunjungan pertama dari Universite de valenciennes Negara Perancis. Mereka menawarkan Program kerjasama di bidang &nbsp;Beasiswa mulai S1,S2 dan S3 yang tidak lepas dari dukungan Kementrian Dikti dan Kedutaan Negara Perancis, &nbsp;dalam kunjungan tersebut hadir Direktur Universite de valenciennes Professor Elhadj Dogheche. berarti telah dibukanya keran kerjasama antar kedua Negara. sumber daya yang berkualitas dan kompeten akan sangat di butuhkan oleh bangsa ini, dalam turut membangun&nbsp; peradapan Asia yang maju di masa datang.penandatangan Mou sebagai payung hukum telah di lakukan oleh Direktur Politeknik Negeri Malang Ir.Tundung Subali Patma, MT dan Pihak dari Univeersite de Valanciennes.(Why)</p>', 'mou-perancis.jpg', '2018-10-21', '13:10:52'),
(7, 1, 'SEMINAR NASIONAL PENGELOLAAN LIMBAH 2018', '<p><strong>HMTK Proudly Present</strong></p>\r\n<p><strong>Seminar Nasional Pengelolaan Limbah 2018</strong></p>\r\n<p><strong>&ldquo;Pengolahan Limbah B3 dalam Industri sebagai Wujud Kepedulian Kelestarian dalam Lingkungan&rdquo;</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Latar Belakang :</strong></p>\r\n<p>Dalam era globalisasi penggunaan bahan kimia semakin bertambah. Penggunaan bahan kimia dapat membantu produksi suatu barang, namun juga dapat menimbulkan efek yang cukup membahayakan untuk mahkluk hidup ataupun lingkungan.</p>\r\n<p>Pengolahan limbah B3 merupakan kegiatan yang bertujuan untuk menghilangkan atau setidaknya menurunkan sifat bahaya dari suatu limbah B3. Saat ini pemanfaatan limbah B3 sangat digalakkan, karena selain dapat menghilangkan sifat bahaya dari limbah B3, kegiatan ini dapat pula membantu menjaga kelestarian lingkungan melalui pengurangan penggunaan sumber daya alam.</p>\r\n<p>&nbsp;</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"../haki_polinema_2/assets/wp-contents/IMG-20181010-WA0016-1.jpg\" alt=\"\" width=\"913\" height=\"1280\" /></p>\r\n<p><strong>Will be held on:</strong></p>\r\n<p>Sabtu, 20&nbsp; Oktober 2018</p>\r\n<p>Aula Pertamina Politeknik Negeri Malang</p>\r\n<p>07.30 &ndash; 15.00 WIB</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Ticket price:</strong></p>\r\n<p>Mahasiswa Polinema IDR 70K</p>\r\n<p>Mahasiswa Umum IDR 75K</p>\r\n<p>Masyarakat Umum IDR 80K</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Facilities:</strong></p>\r\n<p>Sertifikat Nasional</p>\r\n<p>Seminar Kit</p>\r\n<p>Snack</p>\r\n<p>Lunch</p>\r\n<p>Coffe break</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Registration:</strong></p>\r\n<p>Depan Teras Gedung AO (11AM &ndash; 5PM)</p>\r\n<p>Depan Masjid An-Nur Dalam (11AM &ndash; 5PM)</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Via transfer:</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong>Formulir Pendaftaran [click here]</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong>More information:</strong></p>\r\n<p>Line : @dbp9172y</p>\r\n<p>Instagram : @Polinema.HMTK</p>\r\n<p>Facebook : HMTK Polinema</p>\r\n<p>Twitter : @HMTK_Polinema</p>\r\n<p>Email : polinema.hmtk@gmail.com</p>', '', '2018-10-21', '14:10:15'),
(8, 1, 'Lomba Gambar Teknik HMS', '<p style=\"text-align: center;\"><strong>Lomba Gambar Teknik</strong></p>\r\n<p>Merupakan bagian dari serangkaian acara Student Day of Civil Engineering 12<sup>th</sup>&nbsp;berupa kompetisi untuk mahasiswa D3/D4/S1 jurusan teknik sipil dan arsitektur seluruh indonesia berupa lomba Gambar Teknik.</p>\r\n<p>Infrastruktur perkantoran merupakan bagian penting dalam proses perkembangan perekonomian suatu daerah bahkan Negara.&nbsp;Maka dari itu diadakannya Lomba Gambar Teknik &nbsp;ini bertujuan meningkatkan kepedulian seorang mahasiswa bidang konstruksi karena dalam membuat sebuah perencanaan bukan hanya melihat dari sisi materiil tetapi juga tidak mengesampingkan konsep yang nyaman dan berkelanjutan yaitu Sustainable Design. Maka dari itu pada LGT kali ini kami mengangkat tema</p>\r\n<p style=\"text-align: center;\"><strong><em>&ldquo;Perencanaan Gedung Perkantoran Berkonsep Sustainable Design&rdquo;</em></strong></p>\r\n<p>Syarat:</p>\r\n<ol>\r\n<li>Mahasiswa D3/D4/S1 Jurusan Teknik Sipil dan Arsitektur</li>\r\n<li>Dalam 1 tim terdiri dari maksimal 3 orang dan harus dari perguruan tinggi yang sama</li>\r\n<li>Ketentuan lebih lanjut ada dalam Term of Reference</li>\r\n</ol>\r\n<p>Mekanisme Pendaftaran</p>\r\n<ol>\r\n<li>Mendownload form pendaftaran dari web&nbsp;<a href=\"http://hms.polinema.ac.id/\">Himpunan Mahasiswa Sipil</a>&nbsp;dan mengirimkan kembali form yang sudah diisi peserta ke&nbsp;<strong>studentdaycivilpolinema@gmail.com</strong></li>\r\n<li>Peserta melakukan pembayaran via Bank ke nomor rekening 0588080714 atas nama Ranny Anjarwati dan mengirim bukti pembayaran via line di @foz1780p dengan format&nbsp;<strong>SD_LGT_Nama Kelompok_Nama Instansi_Tanggal Pembayaran</strong>&nbsp;contoh: SD_CTC_AWESOMETEAM_Politeknik Negeri Malang_20/08/2017</li>\r\n<li>Peserta mengirimkan seluruh dokumen yang diwajibkan sesuai pada TOR.</li>\r\n<li>Peserta yang lolos akan diumumkan oleh panitia melalui official Account IG: StudentdayCivil dan HMSridgid_polinema serta OALine: @foz1780p</li>\r\n<li>Peserta yang lolos melakukan Technical Meeting acara pada tanggal 1 Desember 2017 dan melakukan babak final pada 2 Desember 2017 di Gedung Teknik Sipil Politeknik Negeri Malang</li>\r\n</ol>\r\n<p>Peserta melakukan registrasi pembayaran terlebih dahulu ke panitia baru mendapatkan password file dokumen</p>\r\n<p>Term of Reference (TOR) dan formulir pendaftaran dapat di download di</p>\r\n<p>TOR :&nbsp;<a href=\"http://bit.ly/2wrWqGm\">disini</a></p>\r\n<p>Formulir Pendaftaran :&nbsp;<a href=\"http://bit.ly/2fAC7xR\">disini</a></p>\r\n<p>Dokumen :&nbsp;<a href=\"http://bit.ly/2wGGoXM\">disini</a></p>\r\n<p>&nbsp;</p>\r\n<p>Informasi lebih lanjut dapat menghubungi</p>\r\n<p>Contact Person:</p>\r\n<ul>\r\n<li>Dinda (082233435711)</li>\r\n<li>Alfian (085704003213)</li>\r\n</ul>\r\n<p>atau dapat menemui panitia di</p>\r\n<p>Sekretariat Himpunan Mahasiswa Teknik Sipil</p>\r\n<p>Gedung AS Politeknik Negeri Malang</p>\r\n<p>Jalan Soekarno Hatta No.9, Malang 65141</p>', '', '2018-10-21', '14:10:47'),
(9, 1, '1660 Mahasiswa Polinema diwisuda dalam WISUDA TAHAP 1 tahun 2018', '<p style=\"text-align: justify;\">Berakhirnya masa studi atau tahun akademik ditandai dengan diadakannya kegiatan pelepasan lulusan yang pada Sabtu (29/09/2018) kemarin, Politeknik Negeri Malang (Polinema) menyelanggarakan Wisuda Tahap 1 Sarjana Terapan dan Diploma tahun 2018. Sekitar 1660 wisudawan yang berbahagia mengikuti seluruh rangkaian ceremonial wisuda dengan antusias. Para wisudawan dan keluarga telah memadati areal sekitar Graha Polinema sejal pukul 06.00 WIB.</p>\r\n<p style=\"text-align: justify;\">Wisuda yang dipimpin oleh ketua Senat Polinema yakni Bapak Dr. Ir. Tundung Subali Patma. M.T. Wisuda yang dihadiri seluruh jajaran Senat beserta Direktur dan para Pembantu Direktur Polinema berlangsung dengan sangat khidmad. Wisudawan Tahap 1 ini terdiri dari 670 orang program Diploma IV dan 972 orang program Diploma III. Para wisudawan ini dilepas secara langsung oleh Direktur Polinema Bapak Drs.Awan Setiawan,MMT, MM.</p>\r\n<p style=\"text-align: justify;\">Dalam sambutan dan ucapan selamat kepada para wisudawan, Direktur Polinema juga menyampaikan harapannya kepada para wisudawan. &ldquo;Kami berharap saudara dapat mengamalkan ilmu dalam profesi saudara secara lebih bertanggungjawab dan berkualitas. Masyarakat telah menunggu karya-karya saudara, dan kembali kepada masyarakat untuk berkarya sesuai dengan disiplin ilmu yang saudara dapatkan selama menempuh pendidikan di kampus ini,&rdquo; tutur Bapak Awan Setiawan dalam sambutannya.</p>\r\n<p style=\"text-align: justify;\">Dalam wisuda kali ini, Polinema melakukan pelepasan lulusan pertama dari dua kelas kerjasama yaitu yang pertama adalah wisudawan kelas ikatan dinas GMF (PT GMF AeroAsia, anak perusahaan PT Garuda Indonesia) dan yang kedua merupakan wisudawan Bidiksiba atau program Beasiswa CSR PT Bukit Asam Tbk. Para wisudawan tersebut merupakan calon karyawan yang siap berkarya di dua perusahaan besar tersebut. Untuk wisudawan PT Bukit Asam Tbk., mereka merupakan putra daerah dari Tanjung Enim dan daerah sekitar di Provinsi Sumatera Selatan, dimana mereka telah dibiayai penuh oleh PT Bukit Asam selama menempuh pendidikan di Polinema. Dan wisudawan Bidiksiba yang lulus tahun ini berjumlah 5 (lima) orang.</p>\r\n<p style=\"text-align: justify;\">Kebanggaan yang peroleh oleh Polinema tidak itu saja, karena pada wisuda tahap 1 ini Polinema juga meluluskan beberapa mahasiswa dengan predikat Cumlaude atau dengan nilai terbaik disetiap jurusannya. Beberapa mahasiswa tersebut antara lain: Adi Pranoto dari D4 Teknik Informatika dengan perolehan IPK 3.93, Erlinda Novita Sari dari D3 Administrasi Bisnis dengan perolehan IPK 3.98 untuk kelas regular serta Ines Husnun Tsaranuha dari program GMF dengan IPk 3.84 dan Andriani Kamila Karsari dari D3 PLN dengan IPK 3.83.</p>\r\n<p style=\"text-align: center;\"><img src=\"../haki_polinema_2/assets/wp-contents/aaaaaaaaaaaaaaaaaaa-768x509.jpg\" alt=\"\" width=\"768\" height=\"509\" /></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 1:&nbsp;Direktur Polinema Bapak Drs.Awan Setiawan,MMT, MM., memberi sambutan dan ucapan elamat kepada para wisudawan</strong></p>\r\n<p style=\"text-align: center;\"><strong><img src=\"../haki_polinema_2/assets/wp-contents/cccccccccccccccccc.jpg\" alt=\"\" width=\"744\" height=\"497\" /></strong></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 2: 1660 Wisudawan menjalani serangkaian prosesi wisuda tahap 1</strong></p>\r\n<p style=\"text-align: center;\"><img src=\"../haki_polinema_2/assets/wp-contents/d85ce19f-e8cb-4347-8beb-7ec559bbfe8b-768x434.jpg\" alt=\"\" width=\"768\" height=\"434\" /></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 3:&nbsp;Wisudawan Program Bidiksiba PT Bukit Asam Tbk dan pimpinan PT Bukit Asam Tbk.</strong></p>', 'cccccccccccccccccc.jpg', '2018-10-21', '14:10:17'),
(10, 1, 'Ceremony Penutupan LDK Mahasiswa Baru Polinema di DIVIF 2 KOSTRAD', '<p style=\"text-align: justify;\">Malang, 18/08/2018. Sabtu pagi para mahasiswa baru Polinema kelas reguler yang telah mengikuti serangkaian kegiatan Latihan Dasar Kepemimpinan (LDK) gelombang ketiga selama enam hari juga mahasiswa baru 2018 kelas ikatan dinas GMF (anak perusahaan PT Garuda Indonesia) yang telah menjalani masa LDK selama dua minggu telah berkumpul di Lapangan tengah DIVIF 2 KOSTRAD Singosari, Malang, sambil memekikkan semangat dengan yel-yel mereka. LDK Polinema 2018 dibagi menjadi tiga gelombang untuk mahasiswa kelas reguler yang masing-masing gelombang diikuti skeitar 1000 mahasiswa. Dan untuk kelas GMF, LDK dilaksanakan di YON ARMED Singosari, Malang.</p>\r\n<p style=\"text-align: center;\"><img src=\"../haki_polinema_2/assets/wp-contents/IMG_3420.jpg\" alt=\"\" width=\"714\" height=\"401\" /></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 1:&nbsp;Persiapan Upacara Penutupan LDK</strong></p>\r\n<p style=\"text-align: justify;\">Upacara dihadiri oleh jajaran Direktur dan Wakil Direktur, serta pimpinan jurusan dan lembaga di Polinema, Pimpinan PT. GMF dan orang tua atau keluarga dari mahasiswa GMF 2018. Pada upacara penutupan juga dilaksankan prosesi pelepasan atribut LDK serta pemberian penghargaan kepada peserta LDK terbaik sejumlah 4 (empat) orang yakni satu putra dan satu putri dari kelas reguler dan satu putra serta satu putri dari kelas GMF.</p>\r\n<p style=\"text-align: center;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://www.polinema.ac.id/wp-content/uploads/2018/08/IMG_3469.jpg\" /></p>\r\n<p style=\"text-align: center;\"><strong>Gambar 2:&nbsp;Pemberian Penghargaan Kepada Mahasiwa Terbaik</strong></p>\r\n<p style=\"text-align: justify;\">Setelah rangkaian upacara ceremonial ditutup oleh Komandan Upacara, terdapat persembahan dari mahasiswa baru GMF dan Mahasiswa angkatan sebelumnya. Mereka menampilkan atraksi Pasukan Baris berbaris dan juga pertunjukkan Tari dari berbagai daerah di Indonesia, antara lain Tari Kecak, Tari Merak dan Tari Kontemporer. Keaneka ragaman budaya yang ditampilkan ini melambangkan Bhineka Tunggal Ika sebagai bagian dari jiwa Bangsa Kita, walaupun berbeda-beda tapi tetap satu jua. Hal ini merupakan implementasi dari salah satu materi yang diberikan pada poses LDK yaitu mengenai wawasan kebangsaan. (Humas).</p>', 'IMG_3420.jpg', '2018-10-21', '15:10:40'),
(11, 1, 'Lomba Baris-Berbaris VI Antar SMA/SMK/MA Se-Jawa Timur', '<p>Lomba Baris Berbaris VI merupakan suatu event yang diselenggarakan oleh Unit Kegiatan Mahasiswa yaitu Resimen Mahasiswa Politeknik Negeri Malang. Event ini diadakan setiap tahun dan diikuti oleh pelajar antar SMA/SMK Se-Jawa Timur. Dalam event tersebut memperebutkan berbagai juara bergengsi dan juga piala bergilir dari Direktur Politeknik Negeri Malang beserta uang pembinaan jutaan rupiah bagi pemenang. Segera daftarkan diri kalian karena kuota terbatas. Untuk info pendaftaran peserta adalah sebagai berikut :</p>\r\n<h2><strong><u>Pendaftaran&nbsp;</u></strong><strong>&gt;&gt;</strong></h2>\r\n<p><strong>Waktu</strong>:<br />Gelombang I :<br />07 September &ndash; 29 September 2018<br />Gelombang II :<br />01 Oktober &ndash; 13 Oktober 2018 (bila kuota masih ada)</p>\r\n<p><strong>Biaya</strong>&nbsp;:<br />Rp 350.000/tim<br />Transfer melalui Bank BCA 8161-225-975 a.n. Reka Amelia, atau datang langsung ke sekretariat.</p>\r\n<p><strong>Tempat</strong>:<br />Markas Komando Resimen Mahasiswa Satuan 874 Politeknik Negeri Malang.<br />Gedung AS Lt.2<br />Jalan Soekarno-Hatta No. 9 Malang.<br />Pukul 09.00-15.00 WIB</p>\r\n<h2><strong><u>Memperebutkan</u>&gt;&gt;</strong></h2>\r\n<p><strong><u><br /></u></strong>Juara Umum,<br />Juara I,<br />Juara II,<br />Juara III,<br />Juara Harapan I,<br />Juara Harapan II,<br />Juara Harapan III,<br />Juara Madya I,<br />Juara Madya II,<br />Juara Madya III,<br />Juara Bina I,<br />Juara Bina II,<br />Juara Bina III,<br />Juara Mula I,<br />Juara Mula II,<br />Juara Mula III,<br />Juara Purwa I,<br />Juara Purwa II,<br />Juara Purwa III,<br />Best Danton,<br />Best Variasi,<br />Best Murni,</p>\r\n<p>&nbsp;</p>\r\n<h2><strong><u>Fasilitas</u></strong>&gt;&gt;</h2>\r\n<p>Piala Bergilir Direktur Politeknik Negeri Malang (bagi Juara Umum),<br />Piala (bagi Juara),<br />Uang Pembinaan (bagi Juara Utama),<br />Cindera mata (tiap sekolah),<br />Sertifikat (bagi peserta dan juara),<br />Makan siang,<br />Makanan ringan (kue),<br />Sticker.</p>\r\n<h2><strong><u>Pelaksanaan</u></strong>&gt;&gt;</h2>\r\n<p><strong>Technical Meeting I :</strong><br />Hari/Tanggal :<br />Minggu, 30 September 2018,<br />Waktu :<br />10.00 WIB &ndash; selesai,<br />Tempat :<br />Auditorium Gedung AB Politeknik Negeri Malang.<br />Jl. Soekarno Hatta No. 9 Malang.</p>\r\n<p><strong>Technical&nbsp;Meeting II :</strong><br />Hari/Tanggal :<br />Minggu, 14 Oktober 2018,<br />Waktu :<br />10.00 WIB &ndash; selesai,<br />Tempat :<br />Auditorium Gedung AB Politeknik Negeri Malang.<br />Jl. Soekarno Hatta No. 9 Malang.</p>\r\n<p><strong>Lomba</strong>&nbsp;:<br />Hari/Tanggal :<br />Sabtu, 03 November 2018<br />Waktu :<br />07.00 WIB &ndash; selesai,<br />Tempat :<br />Graha Politeknik Negeri Malang.</p>\r\n<p><strong>KUOTA TERBATAS!<br />Dengan batas maksimal pengiriman 2 pleton setiap sekolah.</strong></p>\r\n<p>Contact Person :<br />085815699551 (Satria)<br />085780631521 (Umar)<br />085755269152 (Thaufiq)</p>\r\n<p><strong>More info :</strong><br />FB : lbb Polinema<br />Ig : lbbpolinema<br />Website :&nbsp;<a href=\"http://www.polinema.ac.id/\">www.polinema.ac.id</a></p>\r\n<ul>\r\n<li>Untuk info Petunjuk Pelaksanaan bisa download&nbsp;<a href=\"http://www.polinema.ac.id/wp-content/uploads/2018/10/4.-JUKLAK.docxx\">disini.</a></li>\r\n<li>Untuk Surat Delegsi Sekolah dapat dilihat&nbsp;<a href=\"http://www.polinema.ac.id/wp-content/uploads/2018/10/3.-Surat-Permohonan-Delegasi.docx\">disini.</a></li>\r\n<li>Untuk Formulir Pendaftaran bisa download&nbsp;<a href=\"http://www.polinema.ac.id/wp-content/uploads/2018/10/1.-Formulir-Pendaftaran.docx\">disini.</a></li>\r\n<li>Untuk Formulir Peserta bisa download&nbsp;<a href=\"http://www.polinema.ac.id/wp-content/uploads/2018/10/2.-Surat-Pernyataan.docx\">disini.</a></li>\r\n</ul>\r\n<h3><strong>Semua tentang KUALITAS!</strong><br /><strong>Ikuti dan buktikan kualitas tim kalian!</strong><br /><strong>Sudah siapkah untuk menjadi yang terbaik?</strong></h3>', 'PBB-2018.jpeg', '2018-10-21', '15:10:17'),
(13, 1, 'Menristekdikti Resmikan Graha Polinema dan Gedung Kuliah Terpadu â€¢ Saksikan serah terima Masjid Raya An-Nur', '<p style=\"text-align: justify;\">Malang, Polinema &ndash;</p>\r\n<p style=\"text-align: justify;\">Menteri Riset, Teknologi, dan Pendidikan Tinggi (Menristekdikti) Prof. H. Mohamad Nasir, Ak., Ph.D, meresmikan Graha Polinema dan Gedung Kuliah Terpadu, sekaligus menyaksikan penyerahan Aset Masjid Raya An Nur dari Ikatan Alumni (IKA) Politeknik Negeri Malang (Polinema) kepada Direktur Polinema.</p>\r\n<p style=\"text-align: justify;\">Dalam kesempatan tersebut, Direktur Polinema, Drs. Awan Setyawan, MM, menyampaikan secara detail tentang ketiga bangunan tersebut, yaitu Graha Polinema, Gedung Kuliah Terpadu (GKT), dan Masjid Raya An-Nur, kepada Menristekdikti dan undangan yang hadir.</p>\r\n<p style=\"text-align: justify;\">Graha Polinema yang didisain memiliki konsep ramah lingkungan,mengutamakan efisiensi energi/penghematan energi, yang dibangun secara bertahap mulai awal tahun 2012. Memiliki luas gedung 12.920 m2, berlantai 5 termasuk&nbsp;<em>basement</em>, dengan dana pembangunan 130 Milyar. Dimana anggaran pembangunan Graha Polinema, berasal dari dana Rupiah Murni (RM) sebesar 102 Milyar dan dana Badan Layanan Umum (BLU) sebesar 28 Milyar. Pembangunan gedung sebagai salah satu upaya peningkatan mutu proses belajar mengajar di Polinema.</p>\r\n<p style=\"text-align: justify;\">&ldquo;Graha Polinema dimanfaatkan sebagai tempat pusat kegiatan mahasiswa yang dapat menampung 5.000 orang, selain tempat wisuda, olahraga&nbsp;<em>indoor</em>, tempat lomba mahasiswa tingkat nasional (seperti KJI/KBGI/KRI), dan lainnya. Pada lantai 3 dan 4 digunakan untuk ruang perkantoran dan perpustakaan,&rdquo; jelas Direktur Polinema, Drs. Awan Setiawan, MMT., MM, dalam sambutannya.</p>\r\n<p style=\"text-align: justify;\">Sementara Gedung Kuliah Terpadu (GKT) dibangun pada awal tahun 2013 dalam 4 tahap pembangunan. Memiliki luas bangunan 16.275 m2 dan berlantai 8, anggaran pembangunan gedung diperoleh dari Rupiah Murni 97 Milyar dan BLU 13 Milyar, atau total menghabiskan biaya 110 Milyar.</p>\r\n<p style=\"text-align: justify;\">&ldquo;Adanya GKT ini, dapat menyelesaikan persoalan keterbatasan kelas yang ada di jurusan-jurusan. GKT memiliki fasilitas yang lengkap, yaitu Laboratorium, Bengkel, Ruang Kuliah bagi mahasiswa D3, D4, Jurusan Teknik Elektro, Teknik Mesin, Teknik Sipil, Teknologi lnformasi, dan sebagai ruang kuliah untuk mahasiswa S2 Jurusan Teknik Elektro. Selain itu, juga memiliki ruang Auditorium yang dapat menampung 250 orang,&rdquo; urai Awan.</p>\r\n<p style=\"text-align: justify;\">Sedangkan Masjid Raya An-Nur memiliki luas bangunan 2.620 m2, berlantai 3, dan dapat menampung 3.200 jamaah. Tak hanya sebagai tempat ibadah, juga dapat digunakan untuk melakukan kegiatan islami lainnya. Pembangunan masjid dimulai pada tanggal 9 Februari 2014 atau tepat saat Dies Natalis ke-32. &ldquo;Sementara dengan dana 7,8 Milyar diperoleh dari swadana mandiri, yaitu para alumni, mahasiswa, karyawan Polinema, dosen Polinema serta masyarakat umum. Diperkirakan sampai dengan selesainhya pembangunan masjid tersebut akan menghbiskan totalnya sekitar 12 Milyar. Diharapkan setelah diserahkannya aset masjid kepada Polinema, segera dicatatkan dalam Barang Milik Negara (BMN). Insya Allah Masjid Raya An-Nur dapat segera terselesaikan pengerjaannya. Sehingga dapat dimanfaatkan penggunaanya oleh sivitas akademika, serta masyarakat umum sekitarnya,&rdquo; papar Awan.</p>\r\n<p style=\"text-align: justify;\">Peresmian gedung Graha Polinema dan Gedung Kuliah Terpadu, ditandai dengan penekanan tombol di layar LED bersama antara Ketua Senat Polinema Dr. Ir. Tundung Subali Patma, MT, Menristekdikti Prof.. H. Mohamad Nasir, Ak., Ph.D, dan Direktur Polinema, Drs. Awan Setyawan, MM.</p>\r\n<p style=\"text-align: justify;\">Dalam kesempatan tersebut, Menristekdikti mengapresiasi keberhasilan Polinema sebagai pioneer Politeknik se-Indonesia. Dimana Polinema merupakan satu-satunya Politeknik yang sudah BLU. Sehingga status ini diharapkan bisa mendorong dan memotivasi Politeknik lainnya untuk menjadi BLU, dan menjadi langkah awal menuju Politeknik berkualitas.</p>\r\n<p style=\"text-align: justify;\">Usai peresmian, Menristekdikti memberikan kuliah tamu bertemakan &ldquo;Kebijakan Kemristekdikti untuk meningkatkan daya saing bangsa menuju revolusi industri 4.0&rdquo;. (humas/ebes)</p>', '12c.jpg', '2018-10-21', '15:10:56'),
(14, 1, 'Test', '<p>Test <a href=\"http://localhost/haki_polinema_2/assets/wp-contents/d85ce19f-e8cb-4347-8beb-7ec559bbfe8b-768x434.jpg\">Bro.</a>&nbsp;Tambah lagi <a href=\"http://localhost/haki_polinema_2/assets/wp-contents/cccccccccccccccccc.jpg\">boleh</a> nggak</p>', '', '2018-11-06', '14:51:32');

-- --------------------------------------------------------

--
-- Table structure for table `panduan`
--

CREATE TABLE `panduan` (
  `id_panduan` int(11) NOT NULL,
  `nama_panduan` text,
  `tahun_terbit` varchar(4) DEFAULT NULL,
  `berkas` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `panduan`
--

INSERT INTO `panduan` (`id_panduan`, `nama_panduan`, `tahun_terbit`, `berkas`) VALUES
(1, 'Panduan PPM 2018', '2018', 'panduan-PPM-2018.pdf'),
(2, 'Panduan Penelitian', '2016', '11_PANDUAN_PENELITIAN_2016.pdf'),
(3, 'Panduan PKM', '2016', '12_PANDUAN_PkM_2016.pdf'),
(4, 'Format Penulisan Makalah PPM Edisi X Tahun 2016', '2016', 'Panduan_Pelaksanaan_Penelitian_dan_PPM_Edisi_X_2016.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `paten`
--

CREATE TABLE `paten` (
  `id_paten` int(11) NOT NULL,
  `nama_inventor` varchar(100) NOT NULL,
  `judul` text NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `nomor_paten` varchar(20) NOT NULL,
  `tahun` char(4) NOT NULL,
  `berkas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paten`
--

INSERT INTO `paten` (`id_paten`, `nama_inventor`, `judul`, `jenis`, `nomor_paten`, `tahun`, `berkas`) VALUES
(1, 'Akhmad Faizin Dipl. Ing. HTL., MT', 'Penadah Tumpahan dan Buangan Air pada Kincir Air', 'Biasa', 'P00201406491', '2014', 'T0cvWWVrRXkzUjltbklUOXUrckI1QT09.pdf'),
(2, 'Ir. Dwina Moentamaria M.T', 'Polyurethane Foam dengan Biokatalis Lipase dan Aplikasi pada Pembuatan Biodiesel dari Minyak Randu', 'Biasa', 'P00201406487', '2014', 'UUhOTDdwcFMzY2k3NnpjQWZsWHBJQT09.pdf'),
(3, 'Ir. GERARD APONNO, MS.', 'FONDASI DANGKAL BETON BERTULANG PRACETAK SEGMENTAL BERBENTUK HURUF V TERBALIK', 'Biasa', 'P000032876', '2013', 'OERaRTgwUXF6NkZJK3ArOFlHc041dz09.pdf'),
(8, 'Kristina Widjajanti,S.Si.,M.Pd', 'PELEPASAN TINTA PADA KERTAS KORAN', 'Biasa', 'P00201508270', '2015', 'bjNFd004MVVNMi8rRlZaR1hBZm1oUT09.pdf'),
(10, 'Ir. Yulianto, M.T', 'Pengontrol Tekanan Berkarakter untuk Bilik Distilasi Minyak Atsiri', 'Biasa', 'P00201507746', '2015', 'N212Qm9la1NqMGlTVnJjbDNmR0dVUT09.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `pelatihan`
--

CREATE TABLE `pelatihan` (
  `id_pelatihan` int(11) NOT NULL,
  `nama_pelatihan` text,
  `konten` text,
  `tanggal` date DEFAULT NULL,
  `jam` time DEFAULT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelatihan`
--

INSERT INTO `pelatihan` (`id_pelatihan`, `nama_pelatihan`, `konten`, `tanggal`, `jam`, `id_user`) VALUES
(1, 'Pelatihan Dasar HAKI', '<h3>Daftar Materi:</h3>\r\n<table style=\"width: 684px; height: 147px;\" border=\"\" cellspacing=\"\" cellpadding=\"\">\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align: center; width: 26px;\"><strong>No</strong></td>\r\n<td style=\"text-align: center; width: 373px;\"><strong>Judul</strong></td>\r\n<td style=\"text-align: center; width: 263px;\"><strong>Link</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 26px;\">1</td>\r\n<td style=\"width: 373px;\">Materi Pengenalan HKI</td>\r\n<td style=\"width: 263px;\"><a href=\"../../haki_polinema_2/assets/wp-contents/materi_haki_polinema/Pengenalan%20HKI.pdf\" target=\"_blank\" rel=\"noopener\">Download .pdf</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 26px;\">2</td>\r\n<td style=\"width: 373px;\">Penelusuran Paten</td>\r\n<td style=\"width: 263px;\"><a href=\"../../haki_polinema_2/assets/wp-contents/materi_haki_polinema/Searching%20Patent%20(UP).pdf\" target=\"_blank\" rel=\"noopener\">Download .pdf</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 26px;\">3</td>\r\n<td style=\"width: 373px;\">Penulisan Deskripsi Paten</td>\r\n<td style=\"width: 263px;\"><a href=\"../../haki_polinema_2/assets/wp-contents/materi_haki_polinema/DESKRIPSI%20PATEN_ppt%20%5BCompatibility%20Mode%5D%20(UP).pdf\" target=\"_blank\" rel=\"noopener\">Download .pdf</a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 26px;\">4</td>\r\n<td style=\"width: 373px;\">Contoh Draf Paten</td>\r\n<td style=\"width: 263px;\"><a href=\"../../haki_polinema_2/assets/wp-contents/materi_haki_polinema/Deskripsi%20Draft%20Paten%20Feed%20Supplement%20Anabaena%20azollae.pdf\" target=\"_blank\" rel=\"noopener\">Download .pdf</a></td>\r\n</tr>\r\n</tbody>\r\n</table>', '2018-10-17', '01:10:43', 1),
(2, 'Coba Pelatihan', '<p><img src=\"http://haki.polinema.ac.id/assets/wp-contents/204183.png.jpg\" alt=\"\" width=\"637\" height=\"478\" /></p>\r\n<p>jdbsjbcjdsbjfnjkdbnjcfbndsjkbvjkldasbvjabvbajdwvbjdawbvjdawbvjkdsabvjbdasjvbdsajvbdjksbvdjs</p>', '2018-11-12', '14:11:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `judul_pengumuman` text NOT NULL,
  `teks` text,
  `tanggal` date DEFAULT NULL,
  `jam` time DEFAULT NULL,
  `berkas` varchar(100) NOT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `judul_pengumuman`, `teks`, `tanggal`, `jam`, `berkas`, `id_user`) VALUES
(1, 'Perpanjangan Batas Waktu Penerimaan Proposal UBER HKI Tahun 2014 (selambat-lambatnya tanggal 30 Mei 2014, pukul 16.00 WIB)', '<p>Kepada Yth.<br />1.&nbsp; &nbsp;Rektor/Direktur/Ketua Perguruan Tinggi<br />2.&nbsp; &nbsp;Koordinator Kopertis I s.d. XII<br />di Seluruh Indonesia</p>\r\n<p style=\"text-align: justify;\">Menyusul surat kami Nomor 0492/E5.4/HP/2014, tanggal 13 Februari 2014 beserta panduannya, dengan hormat kami sampaikan bahwa dalam rangka memberikan kesempatan kepada pelaksanaan penelitian dan pengabdian kepada masyarakat yang masih belum selesai, kami memandang perlu untuk memperpanjang batas waktu penerimaan proposal UBER HKI diperpanjang hingga tanggal 30 Mei 2014.</p>\r\n<p style=\"text-align: justify;\">Program Bantuan Pendaftaran Paten UBER-HKI tahun 2014 ditujukan bagi pelaksana penelitian dan pengabdian kepada masyarakat yang telah selesai kegiatannya dan siap diajukan pendaftaran patennya. Kegiatan penelitian dan pengabdian kepada masyarakat yang akan diajukan permohonan pendaftaran patennya tidak dibatasi oleh waktu pelaksanaan penelitian dan pengabdian kepada masyarakat yang dilaksanakan.</p>\r\n<p><strong>U</strong><strong>sulan &nbsp;disampaikan &nbsp;selambat-lambatnya tanggal &nbsp;</strong><strong>30 Mei </strong><strong>201</strong><strong>4</strong><strong>, &nbsp;pukul 16</strong><strong>.00 WIB </strong><strong>ke</strong> <strong>a</strong><strong>l</strong><strong>a</strong><strong>mat:</strong></p>\r\n<p align=\"center\">&nbsp;<strong>Di</strong><strong>rektur Penelitian dan Pengabdian kepada Masyarakat<br />U.p Kasubdit HKI dan Publikasi<br />Di</strong><strong>rektorat Jenderal Pendidikan Tinggi</strong><br /><strong> Kementerian Pendidikan dan Kebudayaan<br />G</strong><strong>edung D Ditjen Dikti Lt. 4 Jl. Pintu 1 Senayan, Jakarta</strong><br /><strong> Telepon 021-57946100 ext.0430, 0431, 0434;<br />Email hkipublikasi.dp2m@dikti.go.id</strong></p>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: justify;\">Kepada Koordinator Kopertis agar dapat meneruskan kepada pimpinan perguruan tinggi di lingkungan kerjanya, dan kepada pimpinan perguruan tinggi mohon dengan hormat dapat menyebarluaskan informasi ini.</p>\r\n<p style=\"text-align: justify;\">Terlampir kami sampaikan Panduan Pengusulan Program UBER HKI 2014, yang juga dipublikasikan di <a href=\"http://dikti.kemdiknas.go.id\">http://dikti.go.id</a> untuk dipergunakan sebagaimana mestinya.</p>\r\n<p>&nbsp;</p>\r\n<p>Atas perhatian dan kerjasamanya diucapkan terima kasih.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: right;\">Direktur Penelitian dan Pengabdian Kepada Masyarakat,</p>\r\n<p style=\"text-align: right;\">ttd</p>\r\n<p style=\"text-align: right;\">&nbsp;</p>\r\n<p style=\"text-align: right;\"><strong>Agus Subekti</strong><br />NIP. 196008011984031002</p>\r\n<p>Lampiran File:<br />- <a href=\"http://dp2m.umm.ac.id/files/file/Surat-edaran-Uber-Hki-2014-perpanjang.pdf\">Surat edaran Uber Hki 2014</a><br />- <a href=\"http://dp2m.umm.ac.id/files/file/PANDUAN-UBER-HKI-2014%281%29.pdf\">PANDUAN-UBER-HKI-2014</a></p>', '2018-10-01', '08:14:06', 'Test.jpg', 1),
(11, 'PERPANJANGAN PENERIMAAN PROPOSAL UBER HKI 2012', '<p><a href=\"../haki_polinema_2/assets/wp-contents/PANDUAN%20UBER%20HKI%202012.pdf\"><strong>Download file pendukung berikut</strong></a></p>', '2018-10-19', '00:10:08', 'surat UBER HKI.jpg', 1),
(12, 'INFO TERBARU BAGI MAHASISWA BARU 2018', '<p>Info untuk Mahasiswa Baru dari seruruh Jurusan D-III &amp; D-IV</p>\r\n<p>Mengeni Pengambilan&nbsp;<strong>&nbsp;JAS ALMAMATER&nbsp;</strong>dan Pengukuran Seragam:</p>\r\n<p>Hari : Senin</p>\r\n<p>Tanggal : 08 Oktober 2018</p>\r\n<p>Temapat :</p>\r\n<ul>\r\n<li>Gedung Graha lantai 2 &mdash;-&gt; Pengambilan JAS ALMAMATER&nbsp;(&nbsp;<strong>Untuk Jurusan Akutansi Niaga,&nbsp; Akutansi dan Bahasa Inggris</strong>)</li>\r\n<li>Gedung Graha lantai 1&mdash;-&gt; Pengukuran Seragam (&nbsp;<strong>Untuk Jurusan Akutansi Niaga,&nbsp; Akutansi dan Bahasa Inggris</strong>)</li>\r\n</ul>\r\n<p>Waktu : 08.00 s/d 16.00 WIB</p>', '2018-10-22', '21:41:15', '', 1),
(13, 'Pengumuman Pengambilan Baju Toga', '<p>Malang, 24 September 2018</p>\r\n<p>No.01/Perlengkapan/IX/2018</p>\r\n<p>Perihal. Pengumuman Pengambilan Baju Toga&nbsp;Tahap 1</p>\r\n<p>Kepada Yth. Calon Wisuda Tahap.1</p>\r\n<p>Sehubungan dengan akan dilaksanakan wisuda Tahap 1, pada :</p>\r\n<p>Hari &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Sabtu</p>\r\n<p>Tanggal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 29 September 2018</p>\r\n<p>Di Gedung GRAHA POLINEMA</p>\r\n<p>Maka perlengkapan mulai membagikan Baju Toga pada Wisudawan/Wisudawati mulai pada :</p>\r\n<p>Tanggal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 25 s.d. 27 September 2018</p>\r\n<p>Di GRAHA Polinema Lantai 2 Lewat Lift Sebelah Selatan</p>\r\n<p>Demikian atas Perhatian dan kerjasamanya disampaikan terimakasih.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: right;\">Kaur. Persediaan</p>\r\n<p style=\"text-align: right;\">HERI SUWANTORO, S.Pd</p>', '2018-10-22', '21:53:35', '', 1),
(14, 'JADWAL PENGAMBILAN SERAGAM BKL/LAB MAHASISWA BARU TAHUN 2018', '<p>BISA LIHAT DI&nbsp; &mdash;&ndash;&gt;&gt;&gt;&nbsp;http://spmb.polinema.ac.id/pengumuman/index/view/jadwal-pembagian-seragam-bengkel_lab-mahasiswa-baru-tahun-2018</p>', '2018-10-22', '21:54:57', '', 1),
(15, 'Jadwal Wisuda Tahun 2018', '<p>Diberitahukan kepada seluruh Calon Wisudawan Politeknik Negeri Malang yang sudah dinyatakan LULUS Ujian Tugas Akhir/Skripsi &nbsp;:</p>\r\n<ol>\r\n<li>Pelaksanaan Wisuda\r\n<ul>\r\n<li>Tahap I : tanggal &nbsp;29 September &nbsp;2018</li>\r\n<li>Tahap II : tanggal &nbsp;27 Oktober&nbsp; 2018</li>\r\n</ul>\r\n</li>\r\n<li>Wisudawan &nbsp;tidak dipungut biaya.</li>\r\n<li>Bagi calon Wisudawan yang sudah bebas tanggungan bisa melakukan &nbsp;Entry&nbsp; Data Alumni&nbsp; &nbsp;dan&nbsp; &nbsp;melakukan&nbsp; &nbsp;konfirmasi&nbsp; &nbsp;ke &nbsp;masing2&nbsp; &nbsp;Prodinya&nbsp;&nbsp; untuk &nbsp;memastikan sebagai peserta Wisuda Tahap &nbsp;I &nbsp;atau Tahap &nbsp;II.</li>\r\n<li>Pengambilan &nbsp;UNDANGAN &amp; PIN Wisudawan (Di Jurusan/Program Studi),&nbsp;TOGA &amp; GORDON (dapat di ambil di Gedung Graha Polinema)\r\n<ul>\r\n<li>Tahap I :Tanggal 24 s/d 26 September 2018</li>\r\n<li>Tahap II : Tanggal 22 s/d 24 Oktober 2018</li>\r\n</ul>\r\n</li>\r\n<li>Calon &nbsp;Wisudawan/Wisudawati &nbsp;yang &nbsp;belum&nbsp; &nbsp;menyelesaikan&nbsp; &nbsp;Bebas &nbsp;Tanggungan,&nbsp;ljazah&nbsp;dan Transkrip-nya tidak dapat diterimakan.</li>\r\n</ol>\r\n<p>Pengumuman Selengkapnya dapat di unduh di tautan berikut : [<a href=\"http://www.polinema.ac.id/wp-content/uploads/2018/08/PENGUMUMAN_WISUDA2018.pdf\" target=\"_blank\" rel=\"noopener\">download</a>]</p>', '2018-10-22', '21:59:38', '', 1),
(16, 'Jadwal Daftar Ulang Semester Ganjil Mahasiswa Lama', '<p>Berikut ini jadwal daftar ulang Semester Ganjil Tahun Akademik 2018/2019</p>\r\n<ul>\r\n<li>Pembayaran Biaya Daftar Ulang/UKT tanggal 16-24 Agustus 2018</li>\r\n<li>Mencetak Kartu Rencana Studi (KRS) tanggal&nbsp;16-24 Agustus 2018</li>\r\n<li>Verifikasi &amp; Pengesahan KRS ke DPA tanggal&nbsp;16-24 Agustus 2018</li>\r\n<li>Penyerahan KRS ke Akademik Prodi&nbsp;tanggal&nbsp;16-24 Agustus 2018</li>\r\n<li>Awal Kuliah Semester Ganjil tanggal&nbsp;27 Agustus 2018</li>\r\n</ul>\r\n<p>Pengumuman selengkapnya bisa di download pada link berikut :&nbsp;[<a href=\"http://www.polinema.ac.id/wp-content/uploads/2018/07/Pengumuman-Daful-2018-1-Versi-2.doc\">download</a>]</p>', '2018-10-22', '22:01:25', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(100) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `username`, `password`) VALUES
(1, 'Raka Admiral Abdurrahman', 'raka_matsukaze', 'bladeliger'),
(2, 'Erfan Rohadi, S.T, M.Eng, Ph.D', 'erfan', 'erfan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id_agenda`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `panduan`
--
ALTER TABLE `panduan`
  ADD PRIMARY KEY (`id_panduan`);

--
-- Indexes for table `paten`
--
ALTER TABLE `paten`
  ADD PRIMARY KEY (`id_paten`);

--
-- Indexes for table `pelatihan`
--
ALTER TABLE `pelatihan`
  ADD PRIMARY KEY (`id_pelatihan`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id_agenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `panduan`
--
ALTER TABLE `panduan`
  MODIFY `id_panduan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `paten`
--
ALTER TABLE `paten`
  MODIFY `id_paten` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pelatihan`
--
ALTER TABLE `pelatihan`
  MODIFY `id_pelatihan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `agenda`
--
ALTER TABLE `agenda`
  ADD CONSTRAINT `agenda_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `berita`
--
ALTER TABLE `berita`
  ADD CONSTRAINT `berita_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `pelatihan`
--
ALTER TABLE `pelatihan`
  ADD CONSTRAINT `pelatihan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
