<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/5/18
 * Time: 10:58 AM
 */
    include "../koneksi.php"; include "../session.php";

    $action = $_GET['action'];
    $tanggal = date("Y-m-d"); $jam = date("H:i:s");

    //Get user
    $sql_user = $conn->query("SELECT * FROM user WHERE username LIKE '$user_check'");
    $row_user = $sql_user->fetch_array();
    $id_user = $row_user['id_user'];

    //Form
    $judul_pengumuman = $_POST['judul_pengumuman'];
    $isi = $_POST['isi'];

    if($action == 'create')
    {
        //Upload gambar
        if(empty($_FILES['berkas']['name']))
        {
            //tanpa file
            $conn->query("INSERT INTO pengumuman(judul_pengumuman, teks, tanggal, jam, id_user) VALUES ('$judul_pengumuman', '$isi', '$tanggal', '$jam', $id_user)");
            $_SESSION['status_message'] = "INPUT DATA BERHASIL!";
        }
        else
        {
            //ada file
            $nama_file = $_FILES['berkas']['name'];
            $file_tmp = $_FILES['berkas']['tmp_name'];
            move_uploaded_file($file_tmp,'../../../haki_polinema_2/assets/wp-contents/'.$nama_file);
            chmod($nama_file, 0777);

            $conn->query("INSERT INTO pengumuman(judul_pengumuman, teks, tanggal, jam, berkas, id_user) VALUES ('$judul_pengumuman', '$isi', '$tanggal', '$jam', '$nama_file', $id_user)");
            $_SESSION['status_message'] = "INPUT DATA BERHASIL!";
        }
    }
    else if($action == 'delete')
    {
        $id_pengumuman = $_GET['id_pengumuman'];
        $conn->query("DELETE FROM pengumuman WHERE id_pengumuman = ".$id_pengumuman);
        $_SESSION['status_message'] = "HAPUS DATA BERHASIL!";
    }
    else
    {
        $id_pengumuman = $_POST['id_pengumuman'];
        if(empty($_FILES['berkas']['name']))
        {
            //Tanpa file
            $conn->query("UPDATE pengumuman SET judul_pengumuman = '$judul_pengumuman', teks = '$isi' WHERE id_pengumuman = $id_pengumuman");
            $_SESSION['status_message'] = "EDIT DATA BERHASIL!";
        }
        else
        {
            //ada file
            $nama_file = $_FILES['berkas']['name'];
            $file_tmp = $_FILES['berkas']['tmp_name'];
            move_uploaded_file($file_tmp,'../../../haki_polinema_2/assets/wp-contents/'.$nama_file);
            chmod($nama_file, 0777);

            $conn->query("UPDATE pengumuman SET berkas = '$nama_file', judul_pengumuman = '$judul_pengumuman', teks = '$isi' WHERE id_pengumuman = $id_pengumuman");
            $_SESSION['status_message'] = "EDIT DATA BERHASIL!";
        }
    }

    header("Location: ".$base_url."pengumuman");
?>