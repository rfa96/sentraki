<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/4/18
 * Time: 19:54 PM
 */
    include "../koneksi.php";
    include "../session.php";

    $action = $_GET['action'];

    //Get user
    $sql_user = $conn->query("SELECT * FROM user WHERE username LIKE '$user_check'");
    $row_user = $sql_user->fetch_array();
    $id_user = $row_user['id_user'];

    //Form
    $nama_panduan = $_POST['nama_panduan'];
    $tahun_terbit = $_POST['tahun_terbit'];

    if($action == 'create')
    {
        if(!empty($_FILES['berkas']['name']))
        {
            $nama_file = $_FILES['berkas']['name'];
            $file_tmp = $_FILES['berkas']['tmp_name'];
            $dest_files = "../../assets/wp-contents/panduan/".$nama_file;

            move_uploaded_file($file_tmp, $dest_files);
            chmod($nama_file, 0777);
            $conn->query("INSERT INTO panduan(nama_panduan, tahun_terbit, berkas) VALUES ('$nama_panduan', '$tahun_terbit', '$nama_file')");

            $_SESSION['status_message'] = "INPUT DATA BERHASIL!";
        }
        else
        {
            $conn->query("INSERT INTO panduan(nama_panduan, tahun_terbit) VALUES ('$nama_panduan', '$tahun_terbit')");
            $_SESSION['status_message'] = "INPUT DATA BERHASIL!";
        }
    }
    else if($action == 'delete')
    {
        $id_panduan = $_GET['id_panduan'];
        $conn->query("DELETE FROM panduan WHERE id_panduan = ".$id_panduan);
        $_SESSION['status_message'] = "HAPUS DATA BERHASIL!";
    }
    else
    {
        $id_panduan = $_POST['id_panduan'];
        if(!empty($_FILES['berkas']['name']))
        {
            $nama_file = $_FILES['berkas']['name'];
            $file_tmp = $_FILES['berkas']['tmp_name'];
            $dest_files = "../../assets/wp-contents/panduan/".$nama_file;

            move_uploaded_file($file_tmp, $dest_files);
            chmod($nama_file, 0777);
            $conn->query("UPDATE panduan SET nama_panduan = '$nama_panduan', tahun_terbit = '$tahun_terbit', berkas = '$nama_file' WHERE id_panduan = ".$id_panduan);

            $_SESSION['status_message'] = "EDIT DATA BERHASIL!";
        }
        else
        {
            $conn->query("UPDATE panduan SET nama_panduan = '$nama_panduan', tahun_terbit = '$tahun_terbit' WHERE id_panduan = ".$id_panduan);
            $_SESSION['status_message'] = "EDIT DATA BERHASIL!";
        }
    }

    header("Location: ".$base_url."panduan");
?>
