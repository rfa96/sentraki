<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/4/18
 * Time: 21:09 PM
 */
    include "../koneksi.php";
    include "../session.php";

    $action = $_GET['action'];

    //Get user
    $sql_user = $conn->query("SELECT * FROM user WHERE username LIKE '$user_check'");
    $row_user = $sql_user->fetch_array();
    $id_user = $row_user['id_user'];

    //Form
    $nama_inventor = $_POST['nama_inventor'];
    $judul_paten = $_POST['judul_paten'];
    $jenis_paten = $_POST['jenis_paten'];
    $tahun = $_POST['tahun'];
    $nomor_paten = $_POST['nomor_paten'];

    if($action == 'create')
    {
        if(!empty($_FILES['berkas']['name']))
        {
            $nama_file = $_FILES['berkas']['name'];
            $file_tmp = $_FILES['berkas']['tmp_name'];
            $dest_files = "../../assets/wp-contents/paten/".$nama_file;

            move_uploaded_file($file_tmp, $dest_files);
            chmod($nama_file, 0777);
            $conn->query("INSERT INTO paten(nama_inventor, judul, jenis, nomor_paten, tahun, berkas) VALUES ('$nama_inventor', '$judul_paten', '$jenis_paten', '$nomor_paten', '$tahun', '$nama_file')");

            $_SESSION['status_message'] = "INPUT DATA BERHASIL!";
        }
        else
        {
            $conn->query("INSERT INTO paten(nama_inventor, judul, jenis, nomor_paten, tahun) VALUES ('$nama_inventor', '$judul_paten', '$jenis_paten', '$nomor_paten', '$tahun')");
            $_SESSION['status_message'] = "INPUT DATA BERHASIL!";
        }
    }
    else if($action == 'delete')
    {
        $id_paten = $_GET['id_paten'];
        $conn->query("DELETE FROM paten WHERE id_paten = $id_paten");
        $_SESSION['status_message'] = "HAPUS DATA BERHASIL!";
    }
    else
    {
        $id_paten = $_POST['id_paten'];
        if(!empty($_FILES['berkas']['name']))
        {
            $nama_file = $_FILES['berkas']['name'];
            $file_tmp = $_FILES['berkas']['tmp_name'];
            $dest_files = "../../assets/wp-contents/paten/".$nama_file;

            move_uploaded_file($file_tmp, $dest_files);
            chmod($nama_file, 0777);
            $conn->query("UPDATE paten SET nama_inventor = '$nama_inventor', judul = '$judul_paten', jenis = '$jenis_paten', nomor_paten = '$nomor_paten', tahun = '$tahun', berkas = '$nama_file' WHERE id_paten = ".$id_paten);

            $_SESSION['status_message'] = "EDIT DATA BERHASIL!";
        }
        else
        {
            $conn->query("UPDATE paten SET nama_inventor = '$nama_inventor', judul = '$judul_paten', jenis = '$jenis_paten', nomor_paten = '$nomor_paten', tahun = '$tahun' WHERE id_paten = ".$id_paten);
            $_SESSION['status_message'] = "EDIT DATA BERHASIL!";
        }
    }

    header("Location: ".$base_url."paten");
?>