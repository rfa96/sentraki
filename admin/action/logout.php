<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/2/18
 * Time: 08:13 AM
 */
    session_start();

    if(session_destroy())
    {
        header("Location: ../");
    }