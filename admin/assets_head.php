<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Politeknik Negeri Malang | Raka Admiral Abdurrahman">
<link href="<?= $base_url?>wp-contents/logo-polinema.png" rel="icon">

<link href="<?= $base_url?>lib/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="<?= $base_url?>lib/font-awesome/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?= $base_url?>css/zabuto_calendar.css">
<link rel="stylesheet" type="text/css" href="<?= $base_url?>lib/gritter/css/jquery.gritter.css">
<!-- Custom styles for this template -->
<link href="<?= $base_url?>css/style.css" rel="stylesheet">
<link href="<?= $base_url?>css/style-responsive.css" rel="stylesheet">
<script src="<?= $base_url?>lib/chart-master/Chart.js"></script>

<!-- Table -->
<link href="<?= $base_url?>css/table-responsive.css" rel="stylesheet">
<link href="<?= $base_url?>lib/datatables/datatables.min.css" rel="stylesheet"/>

<!-- TinyMCE -->
<script src="<?= $base_url?>lib/tinymce/tinymce.min.js"></script>