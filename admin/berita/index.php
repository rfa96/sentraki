<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/4/18
 * Time: 15:49 PM
 */
    error_reporting(0);
    include "../koneksi.php"; include "../session.php";
    $_SESSION['main_menu'] = "berita";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>Berita - HAKI Polinema</title>
    </head>
    <body>
        <?php include "../assets_aside.php";?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i> Daftar Berita</h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <a href="form.php?crud=create" class="btn btn-round btn-primary" style="margin-bottom: 10px"><i class="fa fa-plus"></i> TAMBAH BERITA</a>
                        <?php
                            if($_SESSION['status_message'])
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $_SESSION['status_message']; unset($_SESSION['status_message'])?>
                                </div>
                                <?php
                            }
                        ?>
                        <div class="content-panel">
                            <table class="display table table-bordered" id="tabel_berita">
                                <thead>
                                    <tr>
                                        <td>No.</td>
                                        <td>Judul Berita</td>
                                        <td>Konten Berita</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $nomor = 1;
                                        $sql_berita = $conn->query("SELECT * FROM berita");
                                        while($row_berita = $sql_berita->fetch_array())
                                        {
                                            ?>
                                                <tr>
                                                    <td><?php echo $nomor++;?></td>
                                                    <td><?php echo $row_berita['judul']?></td>
                                                    <td><?php echo substr($row_berita['konten_berita'], 0, 100)?>....</td>
                                                    <td align="center">
                                                        <a href="../print.php?bagian=berita&id=<?php echo $row_berita['id_berita']?>" target="_blank" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-print"></i></a>
                                                        <a href="form.php?crud=edit&id_berita=<?php echo $row_berita['id_berita']?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                                        <a href="../action/berita.php?action=delete&id_berita=<?php echo $row_berita['id_berita']?>" onclick="return confirm('Yakin mau menghapus data ini?')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eraser"></i></a>
                                                    </td>
                                                </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
        <script>
            $(document).ready(function () {
                $('#tabel_berita').DataTable();
            })
        </script>
    </body>
</html>
