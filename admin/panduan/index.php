<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/4/18
 * Time: 19:32 PM
 */
    error_reporting(0);
    include "../koneksi.php"; include "../session.php";
    $_SESSION['main_menu'] = "panduan";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>Panduan - HAKI Polinema</title>
    </head>
    <body>
        <?php include "../assets_aside.php";?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i> Daftar Panduan</h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <a href="form.php?crud=create" class="btn btn-round btn-primary" style="margin-bottom: 10px"><i class="fa fa-plus"></i> TAMBAH FILE PANDUAN</a>
                        <?php
                            if($_SESSION['status_message'])
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $_SESSION['status_message']; unset($_SESSION['status_message'])?>
                                </div>
                                <?php
                            }
                        ?>
                        <div class="content-panel">
                            <table class="display table table-bordered" id="tabel_panduan">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Panduan</th>
                                        <th>Terbit Sejak</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $nomor = 1;
                                        $sql_panduan = $conn->query("SELECT * FROM panduan");
                                        while($row_panduan = $sql_panduan->fetch_array())
                                        {
                                            ?>
                                            <tr>
                                                <td><?php echo $nomor++?></td>
                                                <td><?= $row_panduan[1]?></td>
                                                <td><?= $row_panduan[2]?></td>
                                                <td align="center">
                                                    <a href="../../assets/wp-contents/panduan/<?= $row_panduan['berkas']?>" target="_blank" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>
                                                    <a href="form.php?crud=edit&id_panduan=<?php echo $row_panduan[0]?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="../action/panduan.php?action=delete&id_panduan=<?php echo $row_panduan[0]?>" onclick="return confirm('Yakin mau menghapus data ini?')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eraser"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
        <script>
            $(document).ready(function () {
                $('#tabel_panduan').DataTable();
            })
        </script>
    </body>
</html>