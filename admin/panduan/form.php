<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/4/18
 * Time: 19:36 PM
 */
    include "../koneksi.php"; include "../session.php";
    $crud = $_GET['crud'];
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>
            <?php
                if($crud == 'create')
                {
                    echo "Tambah File Panduan";
                }
                else
                {
                    echo "Edit File Panduan";
                }
                ?>
        </title>
    </head>
    <body>
        <?php include "../assets_aside.php"?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i>
                    <?php
                        if($crud == 'create')
                        {
                            echo "Tambah File Panduan";
                        }
                        else
                        {
                            echo "Edit File Panduan";
                        }
                    ?>
                </h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="form-panel">
                            <?php
                                if($crud == 'create')
                                {
                                    ?>
                                        <form method="post" class="form-horizontal style-form" enctype="multipart/form-data" action="../action/panduan.php?action=create">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nama Panduan</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nama_panduan" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Tahun Terbit</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="tahun_terbit" maxlength="4" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Berkas</strong></label><br>
                                                <div class="col-sm-10">
                                                    <input type="file" name="berkas" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="TAMBAH">
                                                    <a href="<?= $base_url?>panduan" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                                else
                                {
                                    $id_panduan = $_GET['id_panduan'];
                                    $sql_panduan = $conn->query("SELECT * FROM panduan WHERE id_panduan = ".$id_panduan);
                                    $row_panduan = $sql_panduan->fetch_array();
                                    ?>
                                        <form method="post" class="form-horizontal style-form" enctype="multipart/form-data" action="../action/panduan.php?action=edit">
                                            <input type="hidden" name="id_panduan" value="<?= $row_panduan[0]?>">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nama Panduan</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nama_panduan" class="form-control" value="<?= $row_panduan[1]?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Tahun Terbit</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="tahun_terbit" maxlength="4" class="form-control" value="<?= $row_panduan[2]?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Berkas</strong></label><br>
                                                <div class="col-sm-10">
                                                    <input type="file" name="berkas">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="EDIT">
                                                    <a href="<?= $base_url?>panduan" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
    </body>
</html>