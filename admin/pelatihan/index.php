<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/5/18
 * Time: 08:57 AM
 */
    error_reporting(0);
    include "../koneksi.php"; include "../session.php";
    $_SESSION['main_menu'] = "pelatihan";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>Pelatihan - HAKI Polinema</title>
    </head>
    <body>
        <?php include "../assets_aside.php";?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i> Daftar Pelatihan</h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <a href="form.php?crud=create" class="btn btn-round btn-primary" style="margin-bottom: 10px"><i class="fa fa-plus"></i> TAMBAH DATA PELATIHAN</a>
                        <?php
                            if($_SESSION['status_message'])
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $_SESSION['status_message']; unset($_SESSION['status_message'])?>
                                </div>
                                <?php
                            }
                        ?>
                        <div class="content-panel">
                            <table class="display table table-bordered" id="tabel_paten">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Pelatihan</th>
                                        <th>Konten</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $nomor = 1;
                                        $sql_pelatihan = $conn->query("SELECT * FROM pelatihan");
                                        while($row_pelatihan = $sql_pelatihan->fetch_array())
                                        {
                                            ?>
                                            <tr>
                                                <td><?= $nomor++?></td>
                                                <td><?= $row_pelatihan[1]?></td>
                                                <td><?= $row_pelatihan[2]?></td>
                                                <td align="center">
                                                    <a href="../print.php?bagian=pelatihan&id=<?php echo $row_pelatihan['id_pelatihan']?>" data-toggle="tooltip" data-placement="top" title="View" target="_blank"><i class="fa fa-print"></i></a>
                                                    <a href="form.php?crud=edit&id_pelatihan=<?php echo $row_pelatihan['id_pelatihan']?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="../action/pelatihan.php?action=delete&id_pelatihan=<?php echo $row_pelatihan['id_pelatihan']?>" onclick="return confirm('Yakin mau menghapus data ini?')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eraser"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
        <script>
            $(document).ready(function () {
                $("#tabel_paten").DataTable();
            });
        </script>
    </body>
</html>
