<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/5/18
 * Time: 09:24 AM
 */
    include "../koneksi.php"; include "../session.php";
    $crud = $_GET['crud'];
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>
            <?php
                if($crud == 'create')
                {
                    echo "Tambah Data Pelatihan";
                }
                else
                {
                    echo "Edit Data Pelatihan";
                }
            ?>
        </title>
        <script>
            tinyMCE.init({
                selector: "textarea",
                theme: "modern",
                setup: function(editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                },
                themes: 'modern',
                height: 200,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
                ],
                toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
                toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
                image_advtab: true,
                relative_urls : false,
                remove_script_host : false,

                external_filemanager_path: "../lib/filemanager/",
                filemanager_title: "File Manager HAKI Polinema" ,
                external_plugins: { "filemanager" : "<?= $base_url?>lib/filemanager/plugin.min.js"},
            });
        </script>
    </head>
    <body>
        <?php include "../assets_aside.php"?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i>
                    <?php
                    if($crud == 'create')
                    {
                        echo "Tambah Data Pelatihan";
                    }
                    else
                    {
                        echo "Edit Data Pelatihan";
                    }
                    ?>
                </h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="form-panel">
                            <?php
                                if($crud == 'create')
                                {
                                    ?>
                                        <form method="post" class="form-horizontal style-form" action="../action/pelatihan.php?action=create" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nama Pelatihan</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="nama_pelatihan" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Konten</strong></label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="konten" required></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="TAMBAH">
                                                    <a href="<?= $base_url?>pelatihan" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                                else
                                {
                                    $id_pelatihan = $_GET['id_pelatihan'];
                                    $sql_pelatihan = $conn->query("SELECT * FROM pelatihan WHERE id_pelatihan = ".$id_pelatihan);
                                    $row_pelatihan = $sql_pelatihan->fetch_array();
                                    ?>
                                        <form method="post" class="form-horizontal style-form" action="../action/pelatihan.php?action=edit" enctype="multipart/form-data">
                                            <input type="hidden" name="id_pelatihan" value="<?= $id_pelatihan?>"/>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nama Pelatihan</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="nama_pelatihan" value="<?= $row_pelatihan[1]?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Konten</strong></label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="konten"><?= $row_pelatihan[2]?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="UBAH">
                                                    <a href="<?= $base_url?>pelatihan" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
    </body>
</html>
