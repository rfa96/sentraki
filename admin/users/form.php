<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/5/18
 * Time: 12:21 PM
 */
    include "../koneksi.php"; include "../session.php";
    $crud = $_GET['crud'];
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>
            <?php
                if($crud == 'create')
                {
                    echo "Tambah User";
                }
                else
                {
                    echo "Edit User";
                }
            ?>
        </title>
    </head>
    <body>
        <?php include "../assets_aside.php"?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i>
                    <?php
                    if($crud == 'create')
                    {
                        echo "Tambah File Paten";
                    }
                    else
                    {
                        echo "Edit File Paten";
                    }
                    ?>
                </h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="form-panel">
                            <?php
                                if($crud == 'create')
                                {
                                    ?>
                                        <form method="post" class="form-horizontal style-form" enctype="multipart/form-data" action="../action/users.php?action=create">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nama Lengkap</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nama_user" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong><i>Username</i></strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="username" class="form-control" required maxlength="20" placeholder="Maksimum 20 karakter tanpa spasi">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Password</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="password" name="password" class="form-control" maxlength="20" placeholder="Maksimum 20 karakter" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="TAMBAH">
                                                    <a href="<?= $base_url?>users" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                                else
                                {
                                    $id_user = $_GET['id_user'];
                                    $sql_user = $conn->query("SELECT * FROM user WHERE id_user = ".$id_user);
                                    $fetch = $sql_user->fetch_array();
                                    ?>
                                        <form method="post" class="form-horizontal style-form" enctype="multipart/form-data" action="../action/users.php?action=edit">
                                            <input type="hidden" name="id_user" value="<?= $id_user?>">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nama Lengkap</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nama_user" class="form-control" value="<?= $fetch['nama_user']?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong><i>Username</i></strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="username" class="form-control" value="<?= $fetch['username']?>" maxlength="20" placeholder="Maksimum 20 karakter tanpa spasi">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Password</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="password" name="password" class="form-control" maxlength="20" placeholder="Max 20 karakter, kosongkan jika tidak ingin diganti">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="EDIT">
                                                    <a href="<?= $base_url?>users" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
    </body>
</html>
