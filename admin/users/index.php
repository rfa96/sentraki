<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/5/18
 * Time: 12:04 PM
 */
    error_reporting(0);
    include "../koneksi.php"; include "../session.php";
    $_SESSION['main_menu'] = "users";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>Pengguna - HAKI Polinema</title>
    </head>
    <body>
        <?php include "../assets_aside.php";?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i> Daftar Users</h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <a href="form.php?crud=create" class="btn btn-round btn-primary" style="margin-bottom: 10px"><i class="fa fa-plus"></i> TAMBAH USER</a>
                        <?php
                            if($_SESSION['status_message'])
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $_SESSION['status_message']; unset($_SESSION['status_message'])?>
                                </div>
                                <?php
                            }
                        ?>
                        <div class="content-panel">
                            <table class="display table table-bordered" id="tabel_users">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama User</th>
                                        <th>Username</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $nomor = 1;
                                        $sql_user = $conn->query("SELECT * FROM user");
                                        while($row_user = $sql_user->fetch_array())
                                        {
                                            ?>
                                                <tr>
                                                    <td align="center"><?= $nomor++?></td>
                                                    <td><?= $row_user['nama_user']?></td>
                                                    <td><?= $row_user['username']?></td>
                                                    <td align="center">
                                                        <a href="form.php?crud=edit&id_user=<?php echo $row_user['id_user']?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                                        <a href="../action/users.php?action=delete&id_user=<?php echo $row_user['id_user']?>" onclick="return confirm('Yakin mau menghapus data ini?')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eraser"></i></a>
                                                    </td>
                                                </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#tabel_users").DataTable();
            });
        </script>
    </body>
</html>
