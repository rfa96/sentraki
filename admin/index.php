<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/1/18
 * Time: 20:56 PM
 */
    error_reporting(0);
    $error = $_GET['error'];
    session_start();
    if(isset($_SESSION['username']))
    {
        header("Location: dashboard.php");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Halaman Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="wp-contents/logo-polinema.png">
        <link rel="stylesheet" type="text/css" href="css/login/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <link rel="stylesheet" type="text/css" href="css/login/vendor/animate/animate.css">
        <link rel="stylesheet" type="text/css" href="css/login/vendor/css-hamburgers/hamburgers.min.css">
        <link rel="stylesheet" type="text/css" href="css/login/vendor/select2/select2.min.css">
        <link rel="stylesheet" type="text/css" href="css/login/css/util.css">
        <link rel="stylesheet" type="text/css" href="css/login/css/main.css">
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
                    <?php
                        if($error == 1)
                        {
                            ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Error!</strong> Username atau password salah!
                                </div>
                            <?php
                        }
                    ?>
                    <form class="login100-form validate-form" method="post" action="action/login.php">
                        <span class="login100-form-title p-b-55">
                            Login
                        </span>

                        <div class="wrap-input100 validate-input m-b-16" data-validate = "Username is required">
                            <input class="input100" type="text" name="username" placeholder="username">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <span class="lnr lnr-user"></span>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
                            <input class="input100" type="password" name="password" placeholder="Password">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <span class="lnr lnr-lock"></span>
                            </span>
                        </div>

                        <div class="container-login100-form-btn p-t-25">
                            <button class="login100-form-btn">
                                Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="css/login/vendor/jquery/jquery-3.2.1.min.js"></script>
        <script src="css/login/vendor/bootstrap/js/popper.js"></script>
        <script src="css/login/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="css/login/vendor/select2/select2.min.js"></script>
        <script src="css/login/js/main.js"></script>
    </body>
</html>
