<section id="container">
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="<?= $base_url?>dashboard.php" class="logo"><b>HAKI<span>&nbsp;POLINEMA</span></b></a>
        <!--logo end-->
        <div class="top-menu">
            <ul class="nav pull-right top-menu">
                <li><a class="logout" href="<?= $base_url?>action/logout.php" onclick="return confirm('Yakin ingin logout?')">Logout</a></li>
            </ul>
        </div>
    </header>
    <aside>
        <div id="sidebar" class="nav-collapse">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
                <p class="centered"><a href="#"><img src="<?= $base_url?>img/users.png" class="img-circle" width="80"></a></p>
                <h5 class="centered"><?= $login_session?></h5>
                <li class="mt">
                    <a class="<?php if($_SESSION['main_menu'] == 'dashboard'){echo 'active';}?>" href="<?= $base_url?>dashboard.php">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="<?php if($_SESSION['main_menu'] == 'agenda'){echo 'active';}?>" href="<?= $base_url?>agenda">
                        <i class="fa fa-newspaper-o"></i>
                        <span>Agenda</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="<?php if($_SESSION['main_menu'] == 'berita'){echo 'active';}?>" href="<?= $base_url?>berita">
                        <i class="fa fa-newspaper-o"></i>
                        <span>Berita</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="<?php if($_SESSION['main_menu'] == 'panduan'){echo 'active';}?>" href="<?= $base_url?>panduan">
                        <i class="fa fa-book"></i>
                        <span>Panduan</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="<?php if($_SESSION['main_menu'] == 'paten'){echo 'active';}?>" href="<?= $base_url?>paten">
                        <i class="fa fa-book"></i>
                        <span>Paten</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="<?php if($_SESSION['main_menu'] == 'pelatihan'){echo 'active';}?>" href="<?= $base_url?>pelatihan">
                        <i class="fa fa-newspaper-o"></i>
                        <span>Pelatihan</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="<?php if($_SESSION['main_menu'] == 'pengumuman'){echo 'active';}?>" href="<?= $base_url?>pengumuman">
                        <i class="fa fa-newspaper-o"></i>
                        <span>Pengumuman</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a class="<?php if($_SESSION['main_menu'] == 'users'){echo 'active';}?>" href="<?= $base_url?>users">
                        <i class="fa fa-user"></i>
                        <span>Users</span>
                    </a>
                </li>
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
</section>