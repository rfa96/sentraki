<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/5/18
 * Time: 10:06 AM
 */
    error_reporting(0);
    include "../koneksi.php"; include "../session.php";
    $_SESSION['main_menu'] = "pengumuman";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>Pengumuman - HAKI Polinema</title>
    </head>
    <body>
        <?php include "../assets_aside.php";?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i> Daftar Pengumuman</h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <a href="form.php?crud=create" class="btn btn-round btn-primary" style="margin-bottom: 10px"><i class="fa fa-plus"></i> TAMBAH PENGUMUMAN</a>
                        <?php
                            if($_SESSION['status_message'])
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $_SESSION['status_message']; unset($_SESSION['status_message'])?>
                                </div>
                                <?php
                            }
                        ?>
                        <div class="content-panel">
                            <table class="display table table-bordered" id="tabel_pengumuman">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Judul</th>
                                        <th>Isi</th>
                                        <th>Berkas</th>
                                        <th>Tanggal | Jam</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $nomor = 1;
                                        $sql_pengumuman = $conn->query("SELECT * FROM pengumuman");
                                        while($row_pengumuman = $sql_pengumuman->fetch_array())
                                        {
                                            ?>
                                                <tr>
                                                    <td><?php echo $nomor++?></td>
                                                    <td><?php echo $row_pengumuman['judul_pengumuman']?></td>
                                                    <td><?php echo substr($row_pengumuman['teks'], 0, 99)?></td>
                                                    <td><?php echo $row_pengumuman['berkas']?></td>
                                                    <td><?= $row_pengumuman['tanggal']." | ".$row_pengumuman['jam']?></td>
                                                    <td align="center">
                                                        <a href="../print.php?bagian=pengumuman&id=<?php echo $row_pengumuman['id_pengumuman']?>" data-toggle="tooltip" data-placement="top" title="View" target="_blank"><i class="fa fa-print"></i></a>
                                                        <a href="form.php?crud=edit&id_pengumuman=<?php echo $row_pengumuman['id_pengumuman']?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                                        <a href="../action/pengumuman.php?action=delete&id_pengumuman=<?php echo $row_pengumuman['id_pengumuman']?>" onclick="return confirm('Yakin mau menghapus data ini?')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eraser"></i></a>
                                                    </td>
                                                </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
        <script>
            $(document).ready(function () {
                $("#tabel_pengumuman").DataTable();
            });
        </script>
    </body>
</html>
