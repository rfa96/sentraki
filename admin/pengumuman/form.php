<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/5/18
 * Time: 10:51 AM
 */
    include "../koneksi.php"; include "../session.php";
    $crud = $_GET['crud'];
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>
            <?php
                if($crud == 'create')
                {
                    echo "Tambah Pengumuman";
                }
                else
                {
                    echo "Edit Pengumuman";
                }
            ?>
        </title>
        <script>
            tinyMCE.init({
                selector: "textarea",
                theme: "modern",
                setup: function(editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                },
                themes: 'modern',
                height: 200,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
                ],
                toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
                toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
                image_advtab: true,
                relative_urls : false,
                remove_script_host : false,

                external_filemanager_path: "../lib/filemanager/",
                filemanager_title: "File Manager HAKI Polinema" ,
                external_plugins: { "filemanager" : "<?= $base_url?>lib/filemanager/plugin.min.js"},
            });
        </script>
    </head>
    <body>
        <?php include "../assets_aside.php"?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i>
                    <?php
                        if($crud == 'create')
                        {
                            echo "Tambah Pengumuman";
                        }
                        else
                        {
                            echo "Edit Pengumuman";
                        }
                    ?>
                </h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="form-panel">
                            <?php
                                if($crud == 'create')
                                {
                                    ?>
                                        <form method="post" class="form-horizontal style-form" action="../action/pengumuman.php?action=create" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Judul</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="judul_pengumuman" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Isi</strong></label>
                                                <div class="col-sm-10">
                                                    <textarea id="textarea_pengumuman" name="isi" rows="3" required></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Berkas</strong></label><br>
                                                <div class="col-sm-10">
                                                    <input type="file" name="berkas">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="TAMBAH">
                                                    <a href="<?= $base_url?>pengumuman" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                                else
                                {
                                    $id_pengumuman = $_GET['id_pengumuman'];
                                    $sql_pengumuman = $conn->query("SELECT * FROM pengumuman WHERE id_pengumuman = ".$id_pengumuman);
                                    $row_pengumuman = $sql_pengumuman->fetch_array();
                                    ?>
                                        <form method="post" class="form-horizontal style-form" enctype="multipart/form-data" action="../action/pengumuman.php?action=edit">
                                            <input type="hidden" name="id_pengumuman" value="<?php echo $row_pengumuman['id_pengumuman']?>">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Judul</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="judul_pengumuman" class="form-control" value="<?php echo $row_pengumuman['judul_pengumuman']?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Isi</strong></label>
                                                <div class="col-sm-10">
                                                    <textarea id="textarea_pengumuman" name="isi" rows="3"><?php echo $row_pengumuman['teks']?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Berkas</strong></label><br>
                                                <div class="col-sm-10">
                                                    <input type="file" name="berkas">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="TAMBAH">
                                                    <a href="<?= $base_url?>pengumuman" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
    </body>
</html>
