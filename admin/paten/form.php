<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/4/18
 * Time: 21:05 PM
 */
    include "../koneksi.php"; include "../session.php";
    $crud = $_GET['crud'];
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>
            <?php
                if($crud == 'create')
                {
                    echo "Tambah File Paten";
                }
                else
                {
                    echo "Edit File Paten";
                }
            ?>
        </title>
    </head>
    <body>
        <?php include "../assets_aside.php"?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i>
                    <?php
                        if($crud == 'create')
                        {
                            echo "Tambah File Paten";
                        }
                        else
                        {
                            echo "Edit File Paten";
                        }
                    ?>
                </h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="form-panel">
                            <?php
                                if($crud == 'create')
                                {
                                    ?>
                                        <form method="post" class="form-horizontal style-form" enctype="multipart/form-data" action="../action/paten.php?action=create">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nomor Paten</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nomor_paten" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Judul Paten</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="judul_paten" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nama Inventor</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nama_inventor" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Jenis Paten</strong></label>
                                                <div class="col-sm-10">
                                                    <select class="col-sm-12" name="jenis_paten">
                                                        <option selected>----- Pilih salah satu -----</option>
                                                        <option value="Biasa">Biasa</option>
                                                        <option value="Sederhana">Sederhana</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Tahun</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="tahun" class="form-control" required maxlength="4">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Berkas</strong></label><br>
                                                <div class="col-sm-10">
                                                    <input type="file" name="berkas">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="TAMBAH">
                                                    <a href="<?= $base_url?>paten" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                                else
                                {
                                    $id_paten = $_GET['id_paten'];
                                    $sql_paten = $conn->query("SELECT * FROM paten WHERE id_paten = $id_paten");
                                    $row_paten = $sql_paten->fetch_array();
                                    ?>
                                        <form method="post" class="form-horizontal style-form" enctype="multipart/form-data" action="../action/paten.php?action=edit">
                                            <input type="hidden" value="<?php echo $row_paten['id_paten']?>" name="id_paten">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nomor Paten</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nomor_paten" class="form-control" value="<?php echo $row_paten['nomor_paten']?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Judul Paten</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="judul_paten" class="form-control" value="<?php echo $row_paten['judul']?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Nama Inventor</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="nama_inventor" class="form-control" value="<?php echo $row_paten['nama_inventor']?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Jenis Paten</strong></label>
                                                <div class="col-sm-10">
                                                    <select class="col-sm-12" name="jenis_paten">
                                                        <option selected>----- Pilih salah satu -----</option>
                                                        <option <?php if($row_paten['jenis'] == 'Biasa') echo 'selected';?> value="Biasa">Biasa</option>
                                                        <option <?php if($row_paten['jenis'] == 'Sederhana') echo 'selected';?> value="Sederhana">Sederhana</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Tahun</strong></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="tahun" class="form-control" value="<?php echo $row_paten['tahun']?>" maxlength="4">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"><strong>Berkas</strong></label><br>
                                                <div class="col-sm-10">
                                                    <input type="file" name="berkas">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="EDIT">
                                                    <a href="<?= $base_url?>paten" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>

    </body>
</html>
