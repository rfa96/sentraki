<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/4/18
 * Time: 20:28 PM
 */
    error_reporting(0);
    include "../koneksi.php"; include "../session.php";
    $_SESSION['main_menu'] = "paten";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>Paten - HAKI Polinema</title>
    </head>
    <body>
        <?php include "../assets_aside.php";?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i> Daftar Paten</h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <a href="form.php?crud=create" class="btn btn-round btn-primary" style="margin-bottom: 10px"><i class="fa fa-plus"></i> TAMBAH FILE PATEN</a>
                        <?php
                            if($_SESSION['status_message'])
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $_SESSION['status_message']; unset($_SESSION['status_message'])?>
                                </div>
                                <?php
                            }
                        ?>
                        <div class="content-panel">
                            <table class="display table table-bordered" id="tabel_paten">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Inventor</th>
                                        <th>Judul</th>
                                        <th>Jenis Paten</th>
                                        <th>Nomor Paten</th>
                                        <th>Tahun</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $nomor = 1;
                                        $sql_paten = $conn->query("SELECT * FROM paten");
                                        while($row_paten = $sql_paten->fetch_array())
                                        {
                                            ?>
                                            <tr>
                                                <td><?php echo $nomor++;?></td>
                                                <td><?php echo $row_paten['nama_inventor']?></td>
                                                <td><?php echo $row_paten['judul']; ?></td>
                                                <td><?php echo $row_paten['jenis']?></td>
                                                <td><?php echo $row_paten['nomor_paten']?></td>
                                                <td><?php echo $row_paten['tahun']?></td>
                                                <td align="center">
                                                    <a href="../../../haki_polinema_2/assets/wp-contents/paten/<?php echo $row_paten['berkas']?>" target="_blank" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></a>
                                                    <a href="form.php?crud=edit&id_paten=<?php echo $row_paten['id_paten']?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="../action/paten.php?action=delete&id_paten=<?php echo $row_paten['id_paten']?>" onclick="return confirm('Yakin mau menghapus data ini?')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eraser"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
        <script>
            $(document).ready(function () {
                $('#tabel_paten').DataTable();
            })
        </script>
    </body>
</html>
