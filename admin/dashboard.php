<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/1/18
 * Time: 21:52 PM
 */
    include "koneksi.php"; include "session.php";
    $_SESSION['main_menu'] = "dashboard";

    //Weather API
    $url = "http://api.openweathermap.org/data/2.5/weather?id=1636722&APPID=2f82ad937ff2ee01b7a808efe543f639&mode=xml&type=accurate&units=metric";
    $getWeather = simplexml_load_file($url);
    $getTemperature = $getWeather->temperature['value'];
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "assets_head.php";?>
        <title>Dashboard - HAKI Polinema</title>
    </head>
    <body>
        <?php include "assets_aside.php";?>
        <section id="main-content">
            <section class="wrapper">
                <div class="row mt">
                    <div class="col-md-4 col-sm-4 mb">
                        <div class="grey-panel pn donut-chart">
                            <div class="grey-header">
                                <h5>JUMLAH PATEN</h5>
                            </div>
                            <canvas id="serverstatus01" height="120" width="120"></canvas>
                            <script>
                                var doughnutData = [{
                                    value: 40,
                                    color: "#FF6B6B"
                                }];
                                var myDoughnut = new Chart(document.getElementById("serverstatus01").getContext("2d")).Doughnut(doughnutData);
                            </script>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6 goleft">
                                    <p>Total:</p>
                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <h2>
                                        <?php
                                            $sql_jumlah_haki = $conn->query("SELECT * FROM paten");
                                            $jumlah_row = $sql_jumlah_haki->num_rows;
                                            echo $jumlah_row;
                                        ?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <!-- /grey-panel -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 main-chart">
                        <div class="weather pn">
                            <i class="fa fa-cloud fa-4x"></i>
                            <h2><?= ceil($getTemperature)?>º C</h2>
                            <h4>MALANG, ID</h4>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "assets_js.php";?>
        <script type="text/javascript">
            $(document).ready(function() {
                var unique_id = $.gritter.add({
                    // (string | mandatory) the heading of the notification
                    title: 'Selmat datang di Halaman Admin HAKI Polinema!',
                    // (string | mandatory) the text inside the notification
                    text: 'Di sini Anda bisa mengontrol konten halaman utama yang terdapat di laman http://haki.polinema.ac.id',
                    // (string | optional) the image to display on the left
                    image: 'img/users.png',
                    // (bool | optional) if you want it to fade out on its own or just sit there
                    sticky: false,
                    // (int | optional) the time you want it to be alive for before fading out
                    time: 8000,
                    // (string | optional) the class name you want to apply to that specific message
                    class_name: 'my-sticky-class'
                });

                return false;
            });
        </script>
    </body>
</html>
