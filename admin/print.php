<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/5/18
 * Time: 14:02 PM
 */
    include "koneksi.php"; include "session.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Cetak</title>
        <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.css"/>
    </head>
    <body onload="window.print()">
        <table border="0" style="width: 100%; margin-top: 10px">
            <tbody>
                <tr>
                    <td align="left" width="12%"><img src="wp-contents/logo-polinema.png" width="150" height="150" alt="Logo Polinema" title="Logo Polinema"></td>
                    <td align="center" width="76%" colspan="2">
                        <div>
                            <div style="font-size:23px; font-family: 'Times New Roman'; font-weight: bolder">KEMENTERIAN RISET, TEKNOLOGI, DAN PENDIDIKAN TINGGI</div>
                            <div style="font-size:23px; font-family: 'Times New Roman'; font-weight: bolder">POLITEKNIK NEGERI MALANG</span></div>
                            <div style="font-size:22px; font-family: 'Times New Roman'">Jalan Soekarno-Hatta No.9 PO Box 04 Malang 65141 </div>
                            <div style="font-size:22px; font-family: 'Times New Roman'">Telp. (0341) 404424 Fax. (0341) 404423<br>http://www.polinema.ac.id</div>
                        </div>
                    </td>
                    <td align="left" width="12%"><img src="wp-contents/logo_iso.png" width="150" height="150" alt="Logo Polinema" title="Logo Polinema"></td>
                </tr>
            </tbody>
        </table>
        <hr style="border-top:solid 1px #000000; border-bottom:solid 1px #000000; line-height:normal; color:#FFFFFF; margin-top:2px;">

        <?php
            $bagian = $_GET['bagian'];
            $id = $_GET['id'];
            $script_sql_semua = "SELECT * FROM $bagian WHERE id_$bagian = $id";
            $sql_semua = $conn->query($script_sql_semua);
            $fetch_semua = $sql_semua->fetch_array();

            if($bagian == 'pengumuman')
            {
                ?>
                <h3 class="text-center" style="font-family: 'Times New Roman'; font-weight: bolder"><?= $fetch_semua[1]?></h3>

                <center><img src="../haki_polinema_2/assets/wp-contents/<?= $fetch_semua['berkas']?>" class="text-center"/></center>

                <font face="Times New Roman" size="4">
                    <p align="justify"><?= $fetch_semua[2]?></p>
                </font>
                <?php
            }
            else if($bagian == 'agenda')
            {
                ?>
                <h3 class="text-center" style="font-family: 'Times New Roman'; font-weight: bolder"><?= $fetch_semua[1]?></h3>
                <font face="Times New Roman" size="4">
                    <p align="justify"><?= $fetch_semua[2]?></p>
                </font>
                <?php
            }
            else if($bagian == 'berita')
            {
                ?>
                <h3 class="text-center" style="font-family: 'Times New Roman'; font-weight: bolder"><?= $fetch_semua[2]?></h3>
                <center><img src="../haki_polinema_2/assets/wp-contents/<?= $fetch_semua['gambar']?>" class="text-center"/></center>
                <font face="Times New Roman" size="4">
                    <p align="justify"><?= $fetch_semua[3]?></p>
                </font>
                <?php
            }
            else
            {
                ?>
                <h3 class="text-center" style="font-family: 'Times New Roman'; font-weight: bolder"><?= $fetch_semua[1]?></h3>
                <font face="Times New Roman" size="4">
                    <p align="justify"><?= $fetch_semua[2]?></p>
                </font>
                <?php
            }
        ?>
        <?php include "assets_js.php"?>
    </body>
</html>
