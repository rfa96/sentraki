<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/3/18
 * Time: 01:52 AM
 */
    include "../koneksi.php"; include "../session.php";

    $crud = $_GET['crud'];
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>
            <?php
                if($crud == 'create')
                {
                    echo "Tambah Agenda";
                }
                else
                {
                    echo "Edit Agenda";
                }
            ?>
        </title>
        <script>
            tinyMCE.init({
                selector: "textarea",
                theme: "modern",
                setup: function(editor) {
                    editor.on('change', function () {
                        editor.save();
                    });
                },
                themes: 'modern',
                height: 200,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
                ],
                toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
                toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
                image_advtab: true,
                
                external_filemanager_path: "../lib/filemanager/",
                filemanager_title: "File Manager HAKI Polinema" ,
                external_plugins: {"filemanager" : "<?= $base_url?>lib/filemanager/plugin.min.js"},
            });
        </script>
    </head>
    <body>
        <?php include "../assets_aside.php"?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i>
                    <?php
                        if($crud == 'create')
                        {
                            echo "Tambah Agenda";
                        }
                        else
                        {
                            echo "Edit Agenda";
                        }
                    ?>
                </h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <div class="form-panel">
                            <?php
                                if($crud == 'create')
                                {
                                    ?>
                                        <form action="../action/agenda.php?action=create" class="form-horizontal style-form" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label">Nama Agenda</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="nama_agenda">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label">Konten Agenda</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="konten_agenda"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="TAMBAH">
                                                    <a href="<?= $base_url?>agenda" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                                else
                                {
                                    $id_agenda = $_GET['id_agenda'];
                                    $sql_agenda = $conn->query("SELECT * FROM agenda WHERE id_agenda = $id_agenda");
                                    $row_agenda = $sql_agenda->fetch_array();
                                    ?>
                                        <form action="../action/agenda.php?action=edit" class="form-horizontal style-form" method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="id_agenda" value="<?php echo $row_agenda['id_agenda']?>">
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label">Nama Agenda</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="nama_agenda" value="<?php echo $row_agenda['nama_agenda']?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label">Konten Agenda</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control" name="konten_agenda"><?php echo $row_agenda['konten_agenda']?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <input type="submit" class="btn btn-primary" value="UBAH">
                                                    <a href="<?= $base_url?>agenda" class="btn btn-danger" onclick="return confirm('Yakin ingin membatalkan?')">BATAL</a>
                                                </div>
                                            </div>
                                        </form>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
    </body>
</html>
