<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 11/2/18
 * Time: 20:34 PM
 */
    error_reporting(0);
    include "../koneksi.php"; include "../session.php";
    $_SESSION['main_menu'] = "agenda";
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include "../assets_head.php";?>
        <title>Agenda - HAKI Polinema</title>
    </head>
    <body>
        <?php include "../assets_aside.php";?>
        <section id="main-content">
            <section class="wrapper">
                <h3><i class="fa fa-angle-right"></i> Daftar Agenda</h3>
                <div class="row mt">
                    <div class="col-lg-12">
                        <a href="form.php?crud=create" class="btn btn-round btn-primary" style="margin-bottom: 10px"><i class="fa fa-plus"></i> TAMBAH AGENDA</a>
                        <?php
                            if($_SESSION['status_message'])
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo $_SESSION['status_message']; unset($_SESSION['status_message'])?>
                                </div>
                                <?php
                            }
                        ?>
                        <div class="content-panel">
                            <table class="display table table-bordered" id="tabel_agenda">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Agenda</th>
                                        <th>Konten Agenda</th>
                                        <th>Tanggal | Jam</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $nomor = 1;
                                        $sql_agenda = $conn->query("SELECT * FROM agenda");
                                        while($row_agenda = $sql_agenda->fetch_array())
                                        {
                                            ?>
                                                <tr>
                                                    <td><?= $nomor++;?></td>
                                                    <td><?= $row_agenda[1]?></td>
                                                    <td><?= substr($row_agenda[2], 0, 99)?></td>
                                                    <td><?= $row_agenda[3]." | ".$row_agenda[4]?></td>
                                                    <td align="center">
                                                        <a href="../print.php?bagian=agenda&id=<?php echo $row_agenda['id_agenda']?>" data-toggle="tooltip" data-placement="top" title="Print" target="_blank"><i class="fa fa-print"></i></a>
                                                        <a href="form.php?crud=edit&id_agenda=<?php echo $row_agenda['id_agenda']?>" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>
                                                        <a href="../action/agenda.php?action=delete&id_agenda=<?php echo $row_agenda['id_agenda']?>" onclick="return confirm('Yakin mau menghapus data ini?')" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-eraser"></i></a>
                                                    </td>
                                                </tr>
                                            <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <?php include "../assets_js.php";?>
        <script>
            $(document).ready(function () {
                $('#tabel_agenda').DataTable();
            })
        </script>
    </body>
</html>
