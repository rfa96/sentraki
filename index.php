<?php include "koneksi.php";?>
<!DOCTYPE html>
<html lang="id-ID">
    <head>
        <?php include "head_tag.php"?>
        <title>Halaman Utama Sentra HAKI Polinema</title>
    </head>
    <body class="home page-template page-template-templates page-template-home page-template-templateshome-php page page-id-11" data-smooth-scroll-offset='80'>
        <?php include "navbar.php";?>

        <!--- Content --->
        <div class="main-container">

            <section class="cover cover-features imagebg space--md">
                <div class="background-image-holder" style="background: url('assets/img/bg-polinema.jpg'); opacity: 1;">
                    <img alt="background" src="assets/img/bg-polinema.jpg"/>
                </div>
                <div class="overlay uno"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9 col-md-7">
                            <h1>
                                HKI POLINEMA
                            </h1>
                            <p class="lead">Selamat datang di Sentra HAKI Politeknik Negeri Malang</p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="negative--top space-bottom--sm border--bottom">
                <div class="container">
                    <div class="row">
                        <?php
                            $sql_pengumuman = $conn->query("SELECT * FROM pengumuman ORDER BY tanggal DESC LIMIT 3");
                            while($data_pengumuman = $sql_pengumuman->fetch_array())
                            {
                                ?>
                                    <div class="col-sm-4">
                                        <div class="feature feature--featured feature-1 boxed bg--white box--shadow announce">
                                            <span class="info">Pengumuman</span>
                                            <h5><a href="pengumuman_view.php?id_pengumuman=<?= $data_pengumuman['id_pengumuman']?>"><?= substr($data_pengumuman[1], 0, 84)?>...</a></h5>
                                            <div class="date"><?= date("d-M-Y", strtotime($data_pengumuman[3]))." | ".$data_pengumuman[4];?></div>
                                        </div>
                                    </div>
                                <?php
                            }
                        ?>
                        <center>
                            <div class="pagination"><h5><a href="pengumuman_list.php" class="btn-line btn--beta">Lihat semua pengumuman</a></h5></div>
                        </center>
                    </div>
                </div>
            </section>

            <section class="space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <center><h4><strong>Berita</strong></h4></center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="masonry">
                                <div class="row">
                                    <div class="masonry__container">
                                        <?php
                                            $sql_berita = $conn->query("SELECT * FROM berita ORDER BY tanggal DESC LIMIT 3");
                                            while($data_berita = $sql_berita->fetch_array())
                                            {
                                                ?>
                                                    <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Marketing">
                                                        <article class="feature feature-1">
                                                            <a href="#" class="block"></a>
                                                            <div class="feature__body boxed boxed--border">
                                                                <span><?= date("d-M-Y", strtotime($data_berita[5]))?></span>
                                                                <h5><?= substr($data_berita[2], 0, 70)?>...</h5>
                                                                <a href="berita_view.php?id_berita=<?= $data_berita[0]?>" class="btn btn-primary">Selengkapnya</a>
                                                            </div>
                                                        </article>
                                                    </div>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                                <center>
                                    <div class="pagination"><h5><a href="berita_list.php" class="btn-line btn--beta">Lihat semua berita</a></h5></div>
                                </center>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="space--sm bg--secondary">
                <div class="container">
                    <div class="row">
                        <h3 class="text-center"><strong>Agenda</strong></h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="masonry">
                                <div class="row">
                                    <div class="masonry__container">
                                        <?php
                                            $sql_agenda = $conn->query("SELECT * FROM agenda ORDER BY id_agenda DESC LIMIT 3");
                                            while($data_agenda = $sql_agenda->fetch_array())
                                            {
                                                ?>
                                                    <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Marketing">
                                                        <article class="feature feature-1">
                                                            <a href="#" class="block"></a>
                                                            <div class="feature__body boxed boxed--border">
                                                                <span><?= date("d-M-Y", strtotime($data_agenda[3]))?></span>
                                                                <h5><?= substr($data_agenda[1], 0, 70)?>...</h5>
                                                                <a href="agenda_view.php?id_agenda=<?= $data_agenda[0]?>" class="btn btn-primary">Selengkapnya</a>
                                                            </div>
                                                        </article>
                                                    </div>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                                <center>
                                    <div class="pagination"><h5><a href="agenda_list.php" class="btn-line btn--beta">Lihat semua agenda</a></h5></div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="intro switchable feature-large bg--dark bg--blue space--xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <img class="" alt="logo" src="assets/img/hki-logo.png" width="80%">
                        </div>
                        <div class="col-sm-8">
                            <h2 class="intro__title">Tentang HAKI Polinema</h2>
                            <p align="justify">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                            <p align="justify">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem... </p>
                            <p><a class="btn-line btn--beta" href="profil.php">Selengkapnya</a></p>
                        </div>
                    </div>
                </div>
            </section>

            <?php include "footer.php";?>
        </div>

        <?php include "assets_js.php";?>
    </body>
</html>