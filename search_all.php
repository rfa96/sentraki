<?php
/**
 * Created by PhpStorm.
 * User: raka_matsukaze
 * Date: 10/19/18
 * Time: 10:53 AM
 */
    include "koneksi.php";

    $katakunci = $_POST['s'];

    //SQL Semua
    $sql_agenda = $conn->query("SELECT * FROM agenda WHERE nama_agenda LIKE '%$katakunci%'");
    $sql_berita = $conn->query("SELECT * FROM berita WHERE judul LIKE '%$katakunci%'");
    $sql_pelatihan = $conn->query("SELECT * FROM pelatihan WHERE nama_pelatihan LIKE '%$katakunci%'");
    $sql_pengumuman = $conn->query("SELECT * FROM pengumuman WHERE judul_pengumuman LIKE '%$katakunci%'");
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Hasil Pencarian - HAKI Polinema</title>
        <?php include "head_tag.php";?>
    </head>
    <body class="home page-template page-template-templates page-template-home page-template-templateshome-php page page-id-11" data-smooth-scroll-offset='80'>
        <?php include "navbar.php";?>

        <div class="main-container">
            <section class="text-center space--xs bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                            <h1>HASIL PENCARIAN</h1>
                        </div>
                    </div>
                </div>
            </section>

            <!--Agenda -->
            <section class="space--sm bg--white">
                <div class="container">
                    <div class="row">
                        <h3 class="text-center"><strong>Agenda</strong></h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="masonry">
                                <div class="row">
                                    <div class="masonry__container">
                                        <?php
                                            if($sql_agenda->num_rows > 0)
                                            {
                                                while($row_agenda = $sql_agenda->fetch_array())
                                                {
                                                    ?>
                                                        <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Marketing">
                                                            <article class="feature feature-1">
                                                                <a href="#" class="block"></a>
                                                                <div class="feature__body boxed boxed--border">
                                                                    <span><?= date("d-M-Y", strtotime($row_agenda[3]))?></span>
                                                                    <h5><?= substr($row_agenda[1], 0, 40)?>...</h5>
                                                                    <a href="agenda_view.php?id_agenda=<?= $row_agenda[0]?>" class="btn btn-primary">Selengkapnya</a>
                                                                </div>
                                                            </article>
                                                        </div>
                                                    <?php
                                                }
                                            }
                                            else
                                            {
                                                ?>
                                                    <center>
                                                        <p style="color: red">No data available from search</p>
                                                    </center>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Berita -->
            <section class="space--sm bg--secondary">
                <div class="container">
                    <div class="row">
                        <h3 class="text-center"><strong>Berita</strong></h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="masonry">
                                <div class="row">
                                    <div class="masonry__container">
                                        <?php
                                            if($sql_berita->num_rows > 0)
                                            {
                                                while($row_berita = $sql_berita->fetch_array())
                                                {
                                                    ?>
                                                        <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Marketing">
                                                            <article class="feature feature-1">
                                                                <a href="#" class="block"></a>
                                                                <div class="feature__body boxed boxed--border">
                                                                    <span><?= date("d-M-Y", strtotime($row_berita[5]))?></span>
                                                                    <h5><?= substr($row_berita[2], 0, 75)?>...</h5>
                                                                    <a href="berita_view.php?id_berita=<?= $row_berita[0]?>" class="btn btn-primary">Selengkapnya</a>
                                                                </div>
                                                            </article>
                                                        </div>
                                                    <?php
                                                }
                                            }
                                            else
                                            {
                                                ?>
                                                    <center>
                                                        <p style="color: red">No data available from search</p>
                                                    </center>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Pelatihan -->
            <section class="space--sm bg--white">
                <div class="container">
                    <div class="row">
                        <h3 class="text-center"><strong>Pelatihan</strong></h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="masonry">
                                <div class="row">
                                    <div class="masonry__container">
                                        <?php
                                            if($sql_pelatihan->num_rows > 0)
                                            {
                                                while($row_pelatihan = $sql_pelatihan->fetch_array())
                                                {
                                                    ?>
                                                        <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Marketing">
                                                            <article class="feature feature-1">
                                                                <a href="#" class="block"></a>
                                                                <div class="feature__body boxed boxed--border">
                                                                    <span><?= date("d-M-Y", strtotime($row_pelatihan[4]))?></span>
                                                                    <h5><?= substr($row_pelatihan[1], 0, 70)?>....</h5>
                                                                    <a href="pelatihan_view.php?id_pelatihan=<?= $row_pelatihan[0]?>" class="btn btn-primary">Selengkapnya</a>
                                                                </div>
                                                            </article>
                                                        </div>
                                                    <?php
                                                }
                                            }
                                            else
                                            {
                                                ?>
                                                    <center>
                                                        <p style="color: red">No data available from search</p>
                                                    </center>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Pengumuman -->
            <section class="space--sm bg--secondary">
                <div class="container">
                    <div class="row">
                        <h3 class="text-center"><strong>Pengumuman</strong></h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="masonry">
                                <div class="row">
                                    <div class="masonry__container">
                                        <?php
                                            if($sql_pengumuman->num_rows > 0)
                                            {
                                                while($row_pengumuman = $sql_pengumuman->fetch_array())
                                                {
                                                    ?>
                                                        <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Marketing">
                                                            <article class="feature feature-1">
                                                                <a href="#" class="block"></a>
                                                                <div class="feature__body boxed boxed--border">
                                                                    <span><?= date("d-M-Y", strtotime($row_pengumuman[3]))?></span>
                                                                    <h5><?= substr($row_pengumuman[1], 0, 34)?>....</h5>
                                                                    <a href="pengumuman_view.php?id_pengumuman=<?= $row_pengumuman[0]?>" class="btn btn-primary">Selengkapnya</a>
                                                                </div>
                                                            </article>
                                                        </div>
                                                    <?php
                                                }
                                            }
                                            else
                                            {
                                                ?>
                                                    <center>
                                                        <p style="color: red">No data available from search</p>
                                                    </center>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php include "footer.php";?>
        </div>

        <?php include "assets_js.php";?>
    </body>
</html>